Name "Time Tracker Installer"
OutFile "Time-Tracker-Installer.exe"
!define INSTALLATIONNAME "Time-Tracker"
!define VERSION "4.15.0.0"

/* Version Information */
VIProductVersion ${VERSION}
VIFileVersion ${VERSION}

VIAddVersionKey "ProductName" "Time Tracker"
VIAddVersionKey "ProductVersion" ${VERSION}
VIAddVersionKey "Comments" ""
VIAddVersionKey "CompanyName" "Sebastian Hofer"
VIAddVersionKey "LegalTrademarks" ""
VIAddVersionKey "LegalCopyright" "� Sebastian Hofer 2021"
VIAddVersionKey "FileDescription" "Time Tracker Installer"


InstallDir $PROGRAMFILES\${INSTALLATIONNAME}
Page directory
Page instfiles

/*
--- Installer ---
*/

Section ""
	SetOutPath $INSTDIR
	
	/* Dateien */
	
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\x64\SQLite.Interop.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\x86\SQLite.Interop.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\CommonServiceLocator.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.Extras.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.Extras.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.Platform.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.Platform.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\GalaSoft.MvvmLight.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\Hardcodet.Wpf.TaskbarNotification.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\Hardcodet.Wpf.TaskbarNotification.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\Newtonsoft.Json.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\Newtonsoft.Json.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\RestSharp.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\RestSharp.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\System.Data.SQLite.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\System.Data.SQLite.xml"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\System.Windows.Interactivity.dll"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\time-tracker.exe"
	File "C:\Users\ALFlex\Documents\Visual Studio 2017\Projects\time-tracker-client\time-tracker\bin\Release\time-tracker.exe.config"
	
	/* Startmen� Verkn�pfungen */
	CreateDirectory "$SMPROGRAMS\${INSTALLATIONNAME}"
	CreateShortCut "$SMPROGRAMS\${INSTALLATIONNAME}\Deinstallieren.lnk" "$INSTDIR\Time-Tracker-Uninstaller.exe" "" "$INSTDIR\Time-Tracker-Uninstaller.exe" 0
	CreateShortCut "$SMPROGRAMS\${INSTALLATIONNAME}\Time Tracker.lnk" "$INSTDIR\time-tracker.exe" "" "$INSTDIR\time-tracker.exe" 0
	
	/* Eintrag bei Programme und Features */
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTALLATIONNAME}" "DisplayName" "Time Tracker"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTALLATIONNAME}" "UninstallString" '"$INSTDIR\Time-Tracker-Uninstaller.exe"'
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTALLATIONNAME}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTALLATIONNAME}" "NoRepair" 1
	
	/* Eintrag im Autostart */
	WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "${INSTALLATIONNAME}" "$INSTDIR\time-tracker.exe"
	
	/* Uninstaller */
	WriteUninstaller $INSTDIR\Time-Tracker-Uninstaller.exe
SectionEnd

/*
--- Uninstaller ---
*/

Section "Uninstall"
	DeleteRegValue HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "${INSTALLATIONNAME}"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTALLATIONNAME}"
	Delete "$INSTDIR\x64\*.*"
	Delete "$INSTDIR\x86\*.*"
	Delete "$INSTDIR\*.*"
	RMDir $INSTDIR
	Delete "$SMPROGRAMS\${INSTALLATIONNAME}\*.*"
	RMDir "$SMPROGRAMS\${INSTALLATIONNAME}"
	Delete "$DOCUMENTS\${INSTALLATIONNAME}\*.*"
	RMDir "$DOCUMENTS\${INSTALLATIONNAME}"
SectionEnd