﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Global
{
    public class Param
    {

#if DEBUG
        /// <summary>
        /// Debugmodus an/aus?
        /// </summary>
        public static bool isDebug = true;
#else
        /// <summary>
        /// Debugmodus an/aus?
        /// </summary>
        public static bool isDebug = false;
#endif

        #region App Parameter

        public const string APP_NAME = "Time-Tracker";

        #endregion

        #region Datenbank Parameter

        private const string DB_NAME = "Datenbank.sqlite";
        public const int DB_VERSION = 10;
        public static string DB_CONNECTION_STRING = "Data Source=" + DB_FILENAME + ";Version=3;";

        public const string DB_TEST_FILENAME = "TestDatenbank.sqlite";
        public static string DB_TEST_CONNECTION_STRING = "Data Source=" + DB_TEST_FILENAME + ";Version=3;";

        public const string DB_BACKUP_UNIQUE_STR = "Sicherung";
        public const int BACKUPS_TO_KEEP = 3;

        /// <summary>
        /// gibt den Datenbankpfad und den Datenbanknamen zurück
        /// </summary>
        /// <returns>Datenbank Pfad + Name</returns>
        public static string DB_FILENAME
        {
            get
            {
                if (isDebug)
                    return DB_NAME;
                else
                    return DB_PATH + DB_NAME;
            }
        }

        /// <summary>
        /// gibt den Datenbank Pfad zurück
        /// </summary>
        /// <returns>Datenbank Pfad</returns>
        public static string DB_PATH
        {
            get
            {
                if (isDebug)
                    return @".\";
                else
                {
                    string homeFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    return homeFolder + @"\" + APP_NAME + @"\";
                }
            }
        }

        /// <summary>
        /// gibt den Backup Datenbankpfad und den Datenbanknamen zurück
        /// </summary>
        public static string DB_BACKUP_FILENAME
        {
            get
            {
                if (isDebug)
                    return DateTime.Today.ToString("yyMMdd") + "_" + DB_BACKUP_UNIQUE_STR + "_" + DB_NAME;
                else
                    return DB_PATH + DateTime.Today.ToString("yyMMdd") + "_" + DB_BACKUP_UNIQUE_STR + "_" + DB_NAME;
            }
        }

        #endregion

        #region Zeiten Parameter

        /// <summary>
        /// ID der 'PC an' / 'PC aus' Zeit
        /// </summary>
        public static int pcOnOffTaskId;

        /// <summary>
        /// ID der aktuell erfassten Zeit. -1 steht für 'nicht angestempelt'.
        /// </summary>
        public static int taskId;

        /// <summary>
        /// Bezeichnung für Filter 'Alle Datensätze' anzeigen
        /// </summary>
        public const string FILTER_ALL_CAPTION = "Alle";

        #endregion

        #region Statistik Parameter

        public const int DAY_STATS_GROUP = 0;
        public const int WEEK_STATS_GROUP = 1;
        public const int MONTH_STATS_GROUP = 2;

        /// <summary>
        /// Statistik Gruppierungen
        /// </summary>
        /// <returns></returns>
        public static List<string> getStatsGroups()
        {
            return new List<string>() { "Tag", "Woche", "Monat" };
        }

        #endregion

        #region Event Parameter

        // TODO: Ist der Platz hier richtig?

        /// <summary>
        /// Ereignis Kategorien. Die Integer Werte dürfen nicht geändert werden.
        /// </summary>
        public enum EventCategories {
            NO_LUNCH = 1,       // keinen Mittag
            HOLIDAY = 2,        // Urlaub
            ILL = 3,            // Krank
            LEGAL_HOLIDAY = 4,  // Feiertag
            HALF_HOLIDAY = 5,   // halber Tag Urlaub
            EDUCATION = 6,      // Fortbildung
        }

        public const string NO_LUNCH_STR = "keinen Mittag";
        public const string HOLIDAY_STR = "Urlaub";
        public const string ILL_STR = "Krank";
        public const string LEGAL_HOLIDAY_STR = "Feiertag";
        public const string HALF_HOLIDAY_STR = "halber Tag Urlaub";
        public const string EDUCATION_STR = "Fortbildung";

        /// <summary>
        /// gibt die Ereignis Kategorien zurück
        /// </summary>
        /// <returns></returns>
        public static List<Models.EventCategory> getEventCategories()
        {
            List<Models.EventCategory> returnList = new List<Models.EventCategory>();

            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.NO_LUNCH, Name = NO_LUNCH_STR });
            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.HOLIDAY, Name = HOLIDAY_STR });
            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.ILL, Name = ILL_STR });
            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.LEGAL_HOLIDAY, Name = LEGAL_HOLIDAY_STR });
            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.HALF_HOLIDAY, Name = HALF_HOLIDAY_STR });
            returnList.Add(new Models.EventCategory() { ID = (int)EventCategories.EDUCATION, Name = EDUCATION_STR });

            return returnList;
        }

        /// <summary>
        /// gibt die Ereignis Kategorie mit der angegebenen ID zurück
        /// </summary>
        /// <returns></returns>
        public static Models.EventCategory getEventCategory(int id)
        {
            return getEventCategories().Find(c => c.ID == id);
        }

        #endregion
    }
}
