﻿using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace time_tracker
{
    /// <summary>
    /// Interaktionslogik für DialogFrame2.xaml
    /// </summary>
    public partial class DialogFrame2 : Window
    {
        public DialogFrame2()
        {
            InitializeComponent();

            // Register Message
            Messenger.Default.Register<DialogWindowCloseMessage>(this, m =>
            {
                this.Close();
            });

            // Zwei Ereignisse an einem Tag funktionieren nicht (bugfix #26)
            Messenger.Default.Register<EventUniqueErrorMessage>(this, m =>
            {
                MessageBox.Show("Tschuldige, zwei Ereignisse an einem Tag können nicht eingefügt werden.",
                    "Upps: Ereignis", MessageBoxButton.OK, MessageBoxImage.Error);
            });

            // Fehler beim Teilen einer Zeit
            Messenger.Default.Register<TaskDivideErrorMessage>(this, m =>
            {
                MessageBox.Show("Tschuldige, es ist ein Fehler beim Teilen der Zeit passiert. Vorgang wird abgebrochen.",
                    "Upps: Zeit", MessageBoxButton.OK, MessageBoxImage.Error);
            });
        }
    }
}
