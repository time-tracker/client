﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    /// <summary>
    /// Leeres Dummy Project
    /// </summary>
    public class ProjectEmtpy : Project
    {
        public ProjectEmtpy() : base()
        {
            Name = Global.Param.FILTER_ALL_CAPTION;
        }
    }
}
