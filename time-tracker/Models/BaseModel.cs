﻿using Newtonsoft.Json;
using System;

namespace time_tracker.Models
{
    /// <summary>
    /// Basisklasse für alle Models, die ein Abbild einer Tabelle sind
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public BaseModel()
        {
            Init();
        }

        /// <summary>
        /// Eindeutige Datensatz ID
        /// </summary>
        public virtual int ID { get; set; }

        /// <summary>
        /// Synced?
        /// </summary>
        [JsonIgnore]
        public int Synced { get; set; }

        /// <summary>
        /// Deleted - gelöscht?
        /// </summary>
        [JsonIgnore]
        public int Deleted { get; set; }

        /// <summary>
        /// Object ID (vom Event Datensatz im Backend)
        /// </summary>
        [JsonIgnore]
        public int ObjectId { get; set; }

        /// <summary>
        /// Vorbelegung
        /// </summary>
        public virtual void Init()
        {
            ID = 0;
            Synced = 0;
            Deleted = 0;
            ObjectId = -1;
        }
    }
}
