﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    /// <summary>
    /// Basisklasse für alle Backend (API) Response
    /// </summary>
    public class BaseResponse
    {
        [DeserializeAs(Name = "Id")]
        [JsonProperty("Id")]
        public int ID { get; set; }

        /// <summary>
        /// default ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ID: " + ID;
        }
    }
}
