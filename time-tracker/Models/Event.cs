﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;

namespace time_tracker.Models
{
    public class Event : BaseModel
    {
        /// <summary>
        /// Vorbelegung
        /// </summary>
        public override void Init()
        {
            base.Init();
            On = DateTime.Now;
            Category = null;
            Note = string.Empty;
        }

        /// <summary>
        /// Zeitpunkt Am
        /// </summary>
        [DeserializeAs(Name = "On")]
        [JsonProperty("On")]
        public DateTime On { get; set; }

        /// <summary>
        /// Kategorie
        /// </summary>
        [JsonIgnore]
        public EventCategory Category { get; set; }

        /// <summary>
        /// Kategorie ID
        /// </summary>
        [DeserializeAs(Name = "Category")]
        [JsonProperty("Category")]
        public int? CategoryID
        {
            get
            {
                if (Category != null)
                    return Category.ID;
                else
                    return null;
            }
        }

        /// <summary>
        /// Notiz / Bemerkung
        /// </summary>
        [DeserializeAs(Name = "Note")]
        [JsonProperty("Note")]
        public string Note { get; set; }

        /// <summary>
        /// default ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ID: " + ID + Environment.NewLine +
                "On: " + On + Environment.NewLine +
                "Category: " + Category + Environment.NewLine +
                "Note: " + Note;
        }
    }

    /// <summary>
    /// Backend Event Response Klasse
    /// </summary>
    public class EventResponse : BaseResponse
    {
    }
}
