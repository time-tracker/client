﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    public class Setting
    {
        public int ID { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
