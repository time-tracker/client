﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    public class TaskCollection : List<Task>
    {
        /// <summary>
        /// Summe der Dauer (Stunden)
        /// </summary>
        public string DurationSum
        {
            get
            {
                long sum = 0;

                foreach (Task t in this)
                    sum += t.Duration;

                if (sum > 0)
                    return Helper.TimeCalc.TicksToStr(sum);
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Summe der Projekt Dauer (Stunden)
        /// </summary>
        public string ProjectDurationSum
        {
            get
            {
                long sum = 0;

                foreach (Task t in this)
                {
                    // nur Projekt Zeiten
                    if (t.Project != null)
                        sum += t.Duration;
                }

                if (sum > 0)
                    return Helper.TimeCalc.TicksToStr(sum);
                else
                    return string.Empty;
            }
        }
    }
}
