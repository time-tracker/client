﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System.Collections.Generic;

namespace time_tracker.Models
{
    public class Project : BaseModel
    {
        /// <summary>
        /// Vorbelegung
        /// </summary>
        public override void Init()
        {
            base.Init();
            Name = string.Empty;
            Active = 1;
            Private = 0;
            Tasks = new List<Task>();
        }

        /// <summary>
        /// Name des Projekts
        /// </summary>
        [DeserializeAs(Name = "Name")]
        [JsonProperty("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Ist das Projekt aktiv?
        /// </summary>
        [DeserializeAs(Name = "Active")]
        [JsonProperty("Active")]
        public int Active { get; set; }

        /// <summary>
        /// Handelt es sich um ein privates Projekt?
        /// </summary>
        [DeserializeAs(Name = "Private")]
        [JsonProperty("Private")]
        public int Private { get; set; }

        /// <summary>
        /// erfasste Zeiten
        /// </summary>
        public List<Task> Tasks { get; private set; }

        /// <summary>
        /// Erfasste Gesamtzeit des Projekts
        /// </summary>
        public string TotalTimeStr
        {
            get
            {
                if (ID > 0)
                    return Helper.TimeCalc.TicksToStr(Controller.ProjectController.getTotalProjectTime(ID));
                else
                    return string.Empty;
            }
        }
    }

    /// <summary>
    /// Backend Project Response Klasse
    /// </summary>
    public class ProjectResponse : BaseResponse
    {
    }
}
