﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    /// <summary>
    /// User Klasse
    /// </summary>
    public class User
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public User()
        {
            Init();
        }

        public void Init()
        {
            ID = 0;
            Email = string.Empty;
            Password = string.Empty;
        }

        /// <summary>
        /// ID
        /// </summary>
        [DeserializeAs(Name = "id")]
        [JsonProperty("id")]
        public int ID { get; set; }

        /// <summary>
        /// E-Mail
        /// </summary>
        [DeserializeAs(Name = "email")]
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Kennwort
        /// </summary>
        [DeserializeAs(Name = "password")]
        [JsonProperty("password")]
        public string Password { get; set; }
    }

    /// <summary>
    /// Backend User Response Klasse
    /// </summary>
    public class UserResponse
    {
        [DeserializeAs(Name = "user")]
        [JsonProperty("user")]
        public User User { get; set; }

        /// <summary>
        /// Eindeutiges Backend Token
        /// </summary>
        [DeserializeAs(Name = "token")]
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// default ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ID: " + User.ID + Environment.NewLine +
                "Email: " + User.Email + Environment.NewLine +
                "Password: " + User.Password + Environment.NewLine +
                "Token: " + Token;
        }
    }
}
