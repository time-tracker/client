﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Windows.Media;

namespace time_tracker.Models
{
    public class Task : BaseModel
    {
        public override void Init()
        {
            base.Init();
            From = DateTime.MinValue;
            To = DateTime.MinValue;
            Note = string.Empty;
            Project = null;
            Booked = 0;
            Location = string.Empty;
        }

        /// <summary>
        /// Zeit Von
        /// </summary>
        [DeserializeAs(Name = "From")]
        [JsonProperty("From")]
        public DateTime From { get; set; }

        /// <summary>
        /// Zeit Bis
        /// </summary>
        [DeserializeAs(Name = "To")]
        [JsonProperty("To")]
        public DateTime To { get; set; }

        /// <summary>
        /// Zeit Typ ID (An/Aus Zeit = 1; Projekt Zeit = 3)
        /// </summary>
        [DeserializeAs(Name = "TypeId")]
        [JsonProperty("TypeId")]
        public int TypeId
        {
            get
            {
                if (Project == null)
                    return 1;
                else
                    return 3;
            }
        }

        /// <summary>
        /// Projekt
        /// </summary>
        [JsonIgnore]
        public Project Project { get; set; }
        
        /// <summary>
        /// Projekt ID
        /// </summary>
        [DeserializeAs(Name = "ProjectId")]
        [JsonProperty("ProjectId")]
        public int? ProjectID
        {
            get
            {
                if (Project != null)
                    return Project.ObjectId;
                else
                    return null;
            }
        }

        /// <summary>
        /// Notiz, Bemerkung
        /// </summary>
        [DeserializeAs(Name = "Note")]
        [JsonProperty("Note")]
        public string Note { get; set; }

        /// <summary>
        /// Ort
        /// </summary>
        [DeserializeAs(Name = "Location")]
        [JsonProperty("Location")]
        public string Location { get; set; }

        /// <summary>
        /// Ist die Zeit gebucht / abgerechnet?
        /// </summary>
        [DeserializeAs(Name = "Booked")]
        [JsonProperty("Booked")]
        public int Booked { get; set; }

        /// <summary>
        /// Dauer (kalkuliert)
        /// </summary>
        public long Duration
        {
            get
            {
                if ((From != null) && (To != null))
                    return (To - From).Ticks;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Dauer String (zur Ausgabe)
        /// </summary>
        public string DurationStr
        {
            get
            {
                if (Duration > 0)
                    return Helper.TimeCalc.TicksToStr((To - From).Ticks);
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Hintergrundfarbe für eine An / Aus Zeit
        /// </summary>
        public Brush IsOnOffTimeColor
        {
            get
            {
                if (Project == null)
                    return new SolidColorBrush(Colors.Gainsboro);
                else
                    return new SolidColorBrush(Colors.White);
            }
        }
    }

    /// <summary>
    /// Backend Task Response Klasse
    /// </summary>
    public class TaskResponse : BaseResponse
    {
    }
}
