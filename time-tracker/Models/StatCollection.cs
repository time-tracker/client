﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    public class StatCollection : List<StatRow>
    {
        /// <summary>
        /// Summe der Überstunden
        /// </summary>
        public string OvertimeSum
        {
            get
            {
                long sum = 0;

                foreach (StatRow r in this)
                    sum += r.Overtime;

                return Helper.TimeCalc.TicksToStr(sum);
            }
        }
    }
}
