﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Models
{
    public class StatRow
    {
        private Controller.SettingController settings = null;
        private int dailyWorkingTime;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public StatRow()
        {
            settings = new Controller.SettingController();
            dailyWorkingTime = settings.getIntParameter(Controller.SettingController.DAILY_WORKING_TIME);
        }

        /// <summary>
        /// Schlüssel der Gruppierung, z.B. 02.09.16, KW35, September
        /// </summary>
        public string GroupKey { get; set; }

        /// <summary>
        /// Gruppe: Tag, Woche, Monat
        /// </summary>
        public int Group { get; set; }

        /// <summary>
        /// PC an/aus Zeit
        /// </summary>
        public long OnOffTime { get; set; }

        /// <summary>
        /// Ausgabe String der PC an/aus Zeit
        /// </summary>
        public string OnOffTimeStr
        {
            get
            {
                if (OnOffTime > 0)
                    return Helper.TimeCalc.TicksToStr(OnOffTime);
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Projekt Zeit
        /// </summary>
        public long ProjectTime { get; set; }

        /// <summary>
        /// Ausgabe String der Projekt Zeit
        /// </summary>
        public string ProjectTimeStr
        {
            get
            {
                if (ProjectTime > 0)
                    return Helper.TimeCalc.TicksToStr(ProjectTime);
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Zeiten durch Ereignisse (Events)
        /// </summary>
        public long EventTime { get; set; }

        /// <summary>
        /// Ausgabe String der Event Zeiten
        /// </summary>
        public string EventTimeStr
        {
            get
            {
                if (EventTime > 0)
                    return Helper.TimeCalc.TicksToStr(EventTime);
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Anteil
        /// </summary>
        public int Share
        {
            get
            {
                if ((OnOffTime > 0) && (ProjectTime > 0))
                {
                    double tmpOnOffTime = Convert.ToDouble(OnOffTime);
                    double tmpProjectTime = Convert.ToDouble(ProjectTime);
                    return Convert.ToInt32(Math.Round((tmpProjectTime / tmpOnOffTime) * 100));
                }
                else
                    return 0;
            }
        }

        /// <summary>
        /// Überstunden
        /// </summary>
        public long Overtime { get; set; }

        /// <summary>
        /// Ausgabe String der Überstunden
        /// </summary>
        public string OvertimeStr
        {
            get
            {
                if (Overtime != 0)
                    return Helper.TimeCalc.TicksToStr(Overtime);
                else
                    return string.Empty;
            }
        }
    }
}
