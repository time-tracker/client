﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using time_tracker.Models;

namespace time_tracker.Controller
{
    public class EventController : BaseController<Event>
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public EventController() : base("`Event`")
        {
        }

        /// <summary>
        /// Liest alle Ereignisse aus (mit Filter)
        /// </summary>
        /// <param name="from">Ereignisse von</param>
        /// <param name="to">Ereignisse bis</param>
        /// <param name="onlyNotSynced">nicht synchronisierte Ereignisse</param>
        /// <returns>Ereignisse</returns>
        public List<Event> GetAll(DateTime from, DateTime to, bool onlyNotSynced = false)
        {
            List<Event> returnValue = new List<Event>();

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;

                string sql = "select * from " + TableName + " ";

                // Wurde eine Datum gesetzt?
                if ((from > DateTime.MinValue) && (to > DateTime.MinValue))
                {
                    // nur wenn das Bis Datum größer ist wie das Von Datum macht die Abfrage Sinn
                    if (to >= from)
                    {
                        // Datums Anpassungen wegen unnötiger Uhrzeit
                        DateTime tmpFrom = from.Date;
                        DateTime tmpTo = to.Date.AddDays(1);

                        sql += " where ";
                        sql += " `On` >= " + Helper.Sqlite.DateTime2Timestamp(tmpFrom) + " and " +
                               " `On` < " + Helper.Sqlite.DateTime2Timestamp(tmpTo) + " ";
                    }
                }

                // nur nicht synchronisierte Ereignisse? 
                if (onlyNotSynced)
                {
                    if (sql.Contains("where") == false)
                        sql += " where ";
                    else
                        sql += " and ";

                    sql += " `Synced` = 0 ";
                }
                else
                {
                    // sonst keine gelöschten Datensätze
                    if (sql.Contains("where") == false)
                        sql += " where ";
                    else
                        sql += " and ";

                    sql += " `Deleted` = 0 ";
                }

                sql += " order by `On` desc";

                cmd.CommandText = sql;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Ereignis laden
                    Event @event = FieldsToModel(reader);

                    if (@event != null)
                    {
                        // zur Ereignis Liste hinzufügen
                        returnValue.Add(@event);
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Liest alle Ereignisse aus
        /// </summary>
        /// <returns></returns>
        public override List<Event> GetAll()
        {
            return GetAll(DateTime.MinValue, DateTime.MinValue);
        }

        /// <summary>
        /// Liest alle nicht synchronisierten Ereignisse aus
        /// </summary>
        /// <returns></returns>
        public override List<Event> GetAllNotSynced()
        {
            return GetAll(DateTime.MinValue, DateTime.MinValue, true);
        }

        /// <summary>
        /// Liest das angegebene Ereignis aus
        /// </summary>
        /// <param name="id">ID des Ereignisses</param>
        /// <returns>Ereignis</returns>
        public override Event GetSingle(int id)
        {
            Event returnValue = null;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from " + TableName + " where ID = " + id;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Zeit laden
                    returnValue = FieldsToModel(reader);
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <inheritdoc />
        protected override Event FieldsToModel(SQLiteDataReader reader)
        {
            Event returnValue = null;

            try
            {
                returnValue = new Event();

                // Pflichtfelder
                returnValue.ID = Convert.ToInt32(reader["ID"]);
                returnValue.On = Helper.Sqlite.Timestamp2DateTime(Convert.ToInt32(reader["On"]));
                returnValue.Category = Global.Param.getEventCategory(Convert.ToInt32(reader["Category"]));
                returnValue.Synced = Convert.ToInt32(reader["Synced"]);
                returnValue.Deleted = Convert.ToInt32(reader["Deleted"]);

                // optionale Felder
                returnValue.Note = Convert.ToString(reader["Note"]);

                if (reader["ObjectId"].Equals(DBNull.Value) == false)
                    returnValue.ObjectId = Convert.ToInt32(reader["ObjectId"]);
                else
                    returnValue.ObjectId = -1;
            }
            catch
            {
                return null;
            }

            return returnValue;
        }

        /// <summary>
        /// Speichert das Ereignis
        /// </summary>
        /// <param name="event">Ereignis</param>
        /// <returns>affected rows oder ID des eingefügten Datensatzes</returns>
        public override int Insert(Event row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into " + TableName + " " +
                    "(`On`, Category, Note, Synced, Deleted, ObjectId) values " +
                    "(@on, @category, @note, @synced, @deleted, @objectId)";

                // Parameter setzen
                cmd.Parameters.Add("on", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.On.Date);
                cmd.Parameters.Add("category", DbType.Int32).Value = row.Category.ID;
                cmd.Parameters.Add("note", DbType.String).Value = row.Note;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("deleted", DbType.Int32).Value = row.Deleted;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // Es wird eig. keine ID sondern nur eine Reihennummer vergeben. Diese Reihennummer 
                // wird hinzugefügt, falls die Zeit hinzugefügt, gespeichert und wieder gelöscht wird 
                // OHNE die Datenbank neu zu öffnen.
                cmd.CommandText = "select last_insert_rowid()";
                object ID = cmd.ExecuteScalar();

                // ID vergeben
                returnValue = Convert.ToInt32(ID);
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Aktualisiert das Ereignis
        /// </summary>
        /// <param name="event">Ereignis</param>
        /// <returns>affected rows oder ID des Datensatzes</returns>
        public override int Update(Event row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update " + TableName + " set " +
                    "`On` = @on, Category = @category, Note = @note, Synced = @synced, ObjectId = @objectId " +
                    "where ID = " + row.ID;

                // Parameter setzen
                cmd.Parameters.Add("on", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.On.Date);
                cmd.Parameters.Add("category", DbType.Int32).Value = row.Category.ID;
                cmd.Parameters.Add("note", DbType.String).Value = row.Note;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // ID zurückgeben
                returnValue = row.ID;
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }
    }
}
