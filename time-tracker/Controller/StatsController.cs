﻿using System;
using System.Collections.Generic;
using System.Linq;
using time_tracker.Helper;

namespace time_tracker.Controller
{
    public class StatsController
    {
        private SettingController settings = null;
        private TaskController taskController = null;
        private EventController eventController = null;
        private DateTime _from;
        private DateTime _to;
        private List<Models.Task> _tasks = null;
        private List<Models.Event> _events = null;
        private Models.StatCollection _statsByDay = null;
        private Models.StatCollection _statsByWeek = null;
        private Models.StatCollection _statsByMonth = null;

        /// <summary>
        /// Werktage (pauschal)
        /// </summary>
        public const int WORKDAYS = 5;

        /// <summary>
        /// Wochen im Monat
        /// </summary>
        public const int WEEKS = 4;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public StatsController()
        {
            settings = new SettingController();
            taskController = new TaskController();
            eventController = new EventController();
            _to = DateTime.Now.AddDays(-1);
            _from = To.AddMonths(-1);
            doDateChange();
        }        

        /// <summary>
        /// Von Datum
        /// </summary>
        public DateTime From
        {
            get { return _from; }
            set
            {
                _from = value;
                doDateChange();
            }
        }

        /// <summary>
        /// Bis Datum
        /// </summary>
        public DateTime To
        {
            get { return _to; }
            set
            {
                _to = value;
                doDateChange();
            }
        }

        /// <summary>
        /// Zeiten (Filter) haben sich geändert Event
        /// </summary>
        private void doDateChange()
        {
            UiServices.SetBusyState();

            getTasks();
            getEvents();
            generateViews();
        }

        /// <summary>
        /// Holt alle Zeiten
        /// </summary>
        private void getTasks()
        {
            if ((_from != null) && (_to != null))
                _tasks = taskController.GetAll(_from, _to);
            else
                _tasks = new List<Models.Task>();
        }

        /// <summary>
        /// Holt alle Ereignisse
        /// </summary>
        private void getEvents()
        {
            if ((_from != null) && (_to != null))
                _events = eventController.GetAll(_from, _to);
            else
                _events = new List<Models.Event>();
        }

        /// <summary>
        /// Zeiten für einen bestimmten Tag holen
        /// </summary>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private List<Models.Task> filterTasks(DateTime workDate)
        {
            return _tasks
                    .Where(t => t.From.Date == workDate.Date && t.To.Date == workDate.Date)
                    .ToList();
        }

        /// <summary>
        /// Event für einen bestimmten Tag holen (es gibt immer nur ein Ereignis pro Tag)
        /// </summary>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private Models.Event filterEvents(DateTime workDate)
        {
            return _events.SingleOrDefault(t => t.On.Date == workDate.Date);
        }

        /// <summary>
        /// Tages-, Wochen-, Monatsansicht generieren
        /// </summary>
        private void generateViews()
        {
            //
            // Tagesansicht
            //
            _statsByDay = new Models.StatCollection();
            DateTime workDate = DateTime.Now;
            Models.StatRow dayRow = null;

            workDate = _to;

            // Kalender rückwärts durchgehen (wegen Sortierung #27)
            while (workDate >= _from)
            {
                // 1. neue Zeile erzeugen
                dayRow = new Models.StatRow();
                dayRow.GroupKey = workDate.ToShortDateString();

                // 2. Zeiten zu diesem Tag holen
                List<Models.Task> tasks = filterTasks(workDate);

                // 3. Zeiten durchgehen
                for (int i = 0; i < tasks.Count; i++)
                {
                    Models.Task task = tasks.ElementAt(i);

                    // Zeiten von privaten Projekten gehen nicht in die Statistik ein
                    if (task.Project != null)
                    {
                        if (task.Project.Private == 1)
                            continue;
                    }

                    // Zeiten berechnen
                    if (task.Project == null)
                        dayRow.OnOffTime += (task.To - task.From).Ticks;
                    else
                        dayRow.ProjectTime += (task.To - task.From).Ticks;
                }

                // 4. Ereignisse zu diesem Tag holen
                Models.Event @event = filterEvents(workDate);

                // 5. Ereignis verarbeiten
                if (@event != null)
                {
                    switch (@event.Category.ID)
                    {
                        case (int)Global.Param.EventCategories.NO_LUNCH:
                            // passiert eigentlich gar nichts
                            break;
                        case (int)Global.Param.EventCategories.HOLIDAY:
                            // tägliche Arbeitszeit der EventTime hinzufügen
                            TimeSpan tsHOLIDAY = new TimeSpan(settings.getIntParameter(SettingController.DAILY_WORKING_TIME), 0, 0);
                            dayRow.EventTime = dayRow.EventTime + tsHOLIDAY.Ticks;
                            break;
                        case (int)Global.Param.EventCategories.HALF_HOLIDAY:
                            // tägliche Arbeitszeit durch zwei Teilen und der EventTime hinzufügen
                            TimeSpan tsHALFHOLIDAY = new TimeSpan((settings.getIntParameter(SettingController.DAILY_WORKING_TIME) / 2), 0, 0);
                            dayRow.EventTime = dayRow.EventTime + tsHALFHOLIDAY.Ticks;
                            break;
                        case (int)Global.Param.EventCategories.ILL:
                            // tägliche Arbeitszeit der EventTime hinzufügen
                            TimeSpan tsILL = new TimeSpan(settings.getIntParameter(SettingController.DAILY_WORKING_TIME), 0, 0);
                            dayRow.EventTime = dayRow.EventTime + tsILL.Ticks;
                            break;
                        case (int)Global.Param.EventCategories.LEGAL_HOLIDAY:
                            // tägliche Arbeitszeit der EventTime hinzufügen
                            TimeSpan tsLEGALHOLIDAY = new TimeSpan(settings.getIntParameter(SettingController.DAILY_WORKING_TIME), 0, 0);
                            dayRow.EventTime = dayRow.EventTime + tsLEGALHOLIDAY.Ticks;
                            break;
                        case (int)Global.Param.EventCategories.EDUCATION:
                            // tägliche Arbeitszeit der EventTime hinzufügen
                            TimeSpan tsEDUCATION = new TimeSpan(settings.getIntParameter(SettingController.DAILY_WORKING_TIME), 0, 0);
                            dayRow.EventTime = dayRow.EventTime + tsEDUCATION.Ticks;
                            break;
                    }
                }
                else
                {
                    // nur wenn eine Zeit vorhanden ist
                    if (dayRow.OnOffTime > 0)
                    {
                        // nur an Wochentagen, nicht Samstag nicht Sonntag!
                        if ((workDate.DayOfWeek != DayOfWeek.Saturday) &&
                            (workDate.DayOfWeek != DayOfWeek.Sunday))
                        {
                            // Mittagspause abziehen
                            TimeSpan t = new TimeSpan(settings.getIntParameter(SettingController.LUNCH_TIME), 0, 0);
                            dayRow.OnOffTime = dayRow.OnOffTime - t.Ticks;
                        }
                    }
                }

                // 7. Überstunden berechnen - nur wenn eine Zeit vorhanden ist
                if (dayRow.OnOffTime > 0)
                {
                    // an Wochentagen hängen die Überstunden von den Zeiten ab
                    if ((workDate.DayOfWeek != DayOfWeek.Saturday) &&
                        (workDate.DayOfWeek != DayOfWeek.Sunday))
                    {
                        TimeSpan t = new TimeSpan(settings.getIntParameter(SettingController.DAILY_WORKING_TIME), 0, 0);
                        dayRow.Overtime = dayRow.OnOffTime - (t.Ticks - dayRow.EventTime);
                    }
                    // am Wochenende sind alle Zeiten Überstunden
                    else
                        dayRow.Overtime = dayRow.OnOffTime;
                }

                // 8. Tag abschließen
                dayRow.Group = Global.Param.DAY_STATS_GROUP;
                _statsByDay.Add(dayRow);

                // 9. vorheriger Tag
                workDate = workDate.AddDays(-1);
            }

            //
            // Wochenansicht
            //
            _statsByWeek = new Models.StatCollection();
            int workWeek = 0;
            Models.StatRow weekRow = null;

            // Tagesansichten durchgehen und kumulieren
            for (int i = 0; i < _statsByDay.Count; i++)
            {
                Models.StatRow row = _statsByDay.ElementAt(i);

                // im ersten Durchlauf
                if (i == 0)
                {
                    workWeek = TimeCalc.getWeek(Convert.ToDateTime(row.GroupKey));

                    // neue Zeile erzeugen
                    weekRow = new Models.StatRow();
                    weekRow.GroupKey = "KW " + workWeek;

                    // Zeiten kumulieren
                    weekRow.OnOffTime += row.OnOffTime;
                    weekRow.ProjectTime += row.ProjectTime;
                    weekRow.EventTime += row.EventTime;
                    weekRow.Overtime += row.Overtime;
                }
                // alle weiteren Durchläufe
                else
                {
                    // Woche abschließen und der Liste hinzufügen
                    if (workWeek != TimeCalc.getWeek(Convert.ToDateTime(row.GroupKey)))
                    {
                        // bugfix:  bei Wochenstatistik keine Mittagspause abziehen, weil schon beim
                        //          Tag abgezogen wurde

                        // Woche abschließen
                        weekRow.Group = Global.Param.WEEK_STATS_GROUP;
                        _statsByWeek.Add(weekRow);

                        workWeek = TimeCalc.getWeek(Convert.ToDateTime(row.GroupKey));

                        // neue Zeile erzeugen
                        weekRow = new Models.StatRow();
                        weekRow.GroupKey = "KW " + workWeek;
                    }                    

                    // Zeiten kumulieren
                    weekRow.OnOffTime += row.OnOffTime;
                    weekRow.ProjectTime += row.ProjectTime;
                    weekRow.EventTime += row.EventTime;
                    weekRow.Overtime += row.Overtime;
                }
            }

            // bugfix: NullReferenceException wenn keine Zeiten vorhanden sind
            if (weekRow != null)
            {
                // TODO: dieser Code kommt zwei mal vor (andere Stelle befindet sich knapp oberhalb)

                // bugfix:  bei Wochenstatistik keine Mittagspause abziehen, weil schon beim
                //          Tag abgezogen wurde

                // letzte Woche nachtragen
                weekRow.Group = Global.Param.WEEK_STATS_GROUP;
                _statsByWeek.Add(weekRow);
            }            

            //
            // Monatsansicht
            //
            _statsByMonth = new Models.StatCollection();
            int workMonth = 0;
            Models.StatRow monthRow = null;

            // Tagesansichten durchgehen und kumulieren
            for (int i = 0; i < _statsByDay.Count; i++)
            {
                Models.StatRow row = _statsByDay.ElementAt(i);

                // im ersten Durchlauf
                if (i == 0)
                {
                    workMonth = Convert.ToDateTime(row.GroupKey).Month;

                    // neue Zeile erzeugen
                    monthRow = new Models.StatRow();
                    monthRow.GroupKey = TimeCalc.getMonthStr(workMonth);

                    // Zeiten kumulieren
                    monthRow.OnOffTime += row.OnOffTime;
                    monthRow.ProjectTime += row.ProjectTime;
                    monthRow.EventTime += row.EventTime;
                    monthRow.Overtime += row.Overtime;
                }
                // alle weiteren Durchläufe
                else
                {
                    // Monat abschließen und der Liste hinzufügen
                    if (workMonth != Convert.ToDateTime(row.GroupKey).Month)
                    {
                        // Monat abschließen
                        monthRow.Group = Global.Param.MONTH_STATS_GROUP;
                        _statsByMonth.Add(monthRow);

                        workMonth = Convert.ToDateTime(row.GroupKey).Month;

                        // neue Zeile erzeugen
                        monthRow = new Models.StatRow();
                        monthRow.GroupKey = TimeCalc.getMonthStr(workMonth);
                    }

                    // Zeiten kumulieren
                    monthRow.OnOffTime += row.OnOffTime;
                    monthRow.ProjectTime += row.ProjectTime;
                    monthRow.EventTime += row.EventTime;
                    monthRow.Overtime += row.Overtime;
                }
            }

            // bugfix: NullReferenceException wenn keine Zeiten vorhanden sind
            if (monthRow != null)
            {
                // TODO: dieser Code kommt zwei mal vor (andere Stelle befindet sich knapp oberhalb)

                // letzten Monat nachtragen
                monthRow.Group = Global.Param.MONTH_STATS_GROUP;
                _statsByMonth.Add(monthRow);
            }
        }

        /// <summary>
        /// Tagesansicht
        /// </summary>
        public Models.StatCollection DailyView
        {
            get
            {
                return _statsByDay;
            }
        }

        /// <summary>
        /// Wochenansicht
        /// </summary>
        public Models.StatCollection WeeklyView
        {
            get
            {
                return _statsByWeek;
            }
        }

        /// <summary>
        /// Monatsansicht
        /// </summary>
        public Models.StatCollection MonthlyView
        {
            get
            {
                return _statsByMonth;
            }
        }
    }
}
