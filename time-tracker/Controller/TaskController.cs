﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using time_tracker.Models;

namespace time_tracker.Controller
{
    public class TaskController : BaseController<Models.Task>
    {
        #region Member

        private ProjectController projectController = null;

        #endregion

        /// <summary>
        /// Konstruktor
        /// </summary>
        public TaskController() : base("`Task`")
        {
            projectController = new ProjectController();
        }

        /// <summary>
        /// Liest alle Zeiten aus
        /// </summary>
        /// <param name="from">Zeiten Von</param>
        /// <param name="to">Zeiten Bis</param>
        /// <param name="onlyProjectTimes">nur Projekt Zeiten</param>
        /// <param name="onlyNotBookedTimes">nur NICHT gebuchte / abgerechnete Zeiten</param>
        /// <param name="onlyProject">nur Zeiten dieses Projekts</param>
        /// <returns>Zeiten</returns>
        public TaskCollection GetAll(DateTime from, DateTime to, bool onlyProjectTimes = false, bool onlyNotBookedTimes = false,
            Project onlyProject = null, string location = null)
        {
            TaskCollection returnValue = new TaskCollection();

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                // nur wenn das Bis Datum größer ist wie das Von Datum macht die Abfrage Sinn
                if (to >= from)
                {
                    // Datums Anpassungen wegen unnötiger Uhrzeit
                    DateTime tmpFrom = from.Date;
                    DateTime tmpTo = to.Date.AddDays(1);

                    db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                    db.Open();

                    SQLiteCommand cmd = new SQLiteCommand();
                    cmd.Connection = db;
                    cmd.CommandType = CommandType.Text;
                    // nur abgeschlossene Zeiten auslesen
                    string sql = "select * from " + TableName + " where " +
                        " `From` > " + Helper.Sqlite.DateTime2Timestamp(tmpFrom) + " and " +
                        " `To` <= " + Helper.Sqlite.DateTime2Timestamp(tmpTo) + " and " +
                        " `To` is not NULL ";

                    // nur Zeiten eines Projekts auslesen?
                    if (onlyProject != null)
                    {
                        // Dummy Projekte ausschließen (Klasse: ProjectEmpty)
                        if (onlyProject.ID > 0)
                            sql += " and ProjectID = " + onlyProject.ID + " ";
                    }

                    // nur Zeiten eines Ortes auslesen?
                    if (string.IsNullOrEmpty(location) == false)
                    {
                        // Dummy Location ausscließen
                        if (location.Equals(Global.Param.FILTER_ALL_CAPTION) == false)
                            sql += " and `Location` = \"" + location + "\" ";
                    }

                    // nur Projekt Zeiten auslesen?
                    if (onlyProjectTimes)
                        sql += " and ProjectID is not NULL ";

                    // nur NICHT Gebuchte / Abgerechnete Zeiten auslesen?
                    if (onlyNotBookedTimes)
                        sql += " and Booked = 0 ";

                    // keine gelöschten Datensätze 
                    sql += " and `Deleted` = 0 ";

                    // Sortierung
                    sql += " order by `From` desc ";

                    cmd.CommandText = sql;

                    SQLiteDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        // Zeit laden
                        Task task = FieldsToModel(reader);

                        if (task != null)
                        {
                            // zur Zeiten Liste hinzufügen
                            returnValue.Add(task);
                        }
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Liest alle nicht synchronisierten Tasks aus
        /// </summary>
        /// <returns></returns>
        public override List<Task> GetAllNotSynced()
        {
            List<Task> returnValue = new List<Task>();

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                    
                // nur nicht synchronisierte Zeiten auslesen
                string sql = "select * from " + TableName + " where " +
                    " `Synced` = 0 and " +
                    " `To` is not NULL " +
                    " order by `From` desc ";

                cmd.CommandText = sql;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Zeit laden
                    Task task = FieldsToModel(reader);

                    if (task != null)
                    {
                        // zur Zeiten Liste hinzufügen
                        returnValue.Add(task);
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Liest die angegebene Zeit aus
        /// </summary>
        /// <param name="id">ID der Zeit</param>
        /// <returns>Zeit</returns>
        public override Task GetSingle(int id)
        {
            Task returnValue = null;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from " + TableName + " where ID = " + id;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Zeit laden
                    returnValue = FieldsToModel(reader);
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <inheritdoc />
        protected override Task FieldsToModel(SQLiteDataReader reader)
        {
            Task returnValue = null;

            try
            {
                returnValue = new Task();

                // Pflichtfelder
                returnValue.ID = Convert.ToInt32(reader["ID"]);
                returnValue.From = Helper.Sqlite.Timestamp2DateTime(Convert.ToInt32(reader["From"]));
                returnValue.Synced = Convert.ToInt32(reader["Synced"]);
                returnValue.Deleted = Convert.ToInt32(reader["Deleted"]);

                // optionale Felder
                if (reader["ProjectID"].Equals(DBNull.Value) == false)
                {
                    int projectId = Convert.ToInt32(reader["ProjectID"]);
                    returnValue.Project = projectController.GetSingle(projectId);
                }
                if (reader["To"].Equals(DBNull.Value) == false)
                    returnValue.To = Helper.Sqlite.Timestamp2DateTime(Convert.ToInt32(reader["To"]));

                if (reader["ObjectId"].Equals(DBNull.Value) == false)
                    returnValue.ObjectId = Convert.ToInt32(reader["ObjectId"]);
                else
                    returnValue.ObjectId = -1;

                returnValue.Note = Convert.ToString(reader["Note"]);
                returnValue.Booked = Convert.ToInt32(reader["Booked"]);
                returnValue.Location = Convert.ToString(reader["Location"]);
            }
            catch
            {
                return null;
            }

            return returnValue;
        }

        /// <summary>
        /// Speichert die angegebene Zeit
        /// </summary>
        /// <param name="task">Zeit</param>
        /// <returns>affected rows oder ID des eingefügten Datensatzes</returns>
        public override int Insert(Task row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into " + TableName + " " +
                    "(`From`, `To`, ProjectID, Note, Booked, Location, Synced, Deleted, ObjectId) values " +
                    "(@from, @to, @projectId, @note, @booked, @location, @synced, @deleted, @objectId)";

                // Parameter setzen
                cmd.Parameters.Add("from", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.From);

                if (row.To != DateTime.MinValue)
                    cmd.Parameters.Add("to", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.To);
                else
                    cmd.Parameters.Add("to", DbType.Int32).Value = DBNull.Value;

                cmd.Parameters.Add("note", DbType.String).Value = row.Note;
                cmd.Parameters.Add("booked", DbType.Int32).Value = row.Booked;

                if (row.Project == null)
                    cmd.Parameters.Add("projectId", DbType.Int32).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("projectId", DbType.Int32).Value = row.Project.ID;

                cmd.Parameters.Add("location", DbType.String).Value = row.Location;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("deleted", DbType.Int32).Value = row.Deleted;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // Es wird eig. keine ID sondern nur eine Reihennummer vergeben. Diese Reihennummer 
                // wird hinzugefügt, falls die Zeit hinzugefügt, gespeichert und wieder gelöscht wird 
                // OHNE die Datenbank neu zu öffnen.
                cmd.CommandText = "select last_insert_rowid()";
                object ID = cmd.ExecuteScalar();

                // ID vergeben
                returnValue = Convert.ToInt32(ID);
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Aktualisiert die angegebene Zeit
        /// </summary>
        /// <param name="task">Zeit</param>
        /// <returns>affected rows oder ID des Datensatzes</returns>
        public override int Update(Task row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update " + TableName + " set " +
                    " `From` = @from, `To` = @to, ProjectID = @projectId, Note = @note, Booked = @booked, Location = @location, " +
                    " Synced = @synced, ObjectId = @objectId " +
                    " where ID = " + row.ID;

                // Parameter setzen
                cmd.Parameters.Add("from", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.From);
                cmd.Parameters.Add("to", DbType.Int32).Value = Helper.Sqlite.DateTime2Timestamp(row.To);
                cmd.Parameters.Add("note", DbType.String).Value = row.Note;
                cmd.Parameters.Add("booked", DbType.Int32).Value = row.Booked;
                if (row.Project == null)
                    cmd.Parameters.Add("projectId", DbType.Int32).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("projectId", DbType.Int32).Value = row.Project.ID;

                cmd.Parameters.Add("location", DbType.String).Value = row.Location;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // ID zurückgeben
                returnValue = row.ID;
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// offene Zeiten bereinigen
        /// </summary>
        public int CleanupTasks()
        {
            int returnValue = 0;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "delete from " + TableName + " where `To` is NULL"; 

                returnValue = cmd.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// unterschiedliche Orte auslesen
        /// </summary>
        /// <returns></returns>
        public List<string> GetLocations()
        {
            List<string> returnValue = new List<string>();

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;

                // unterschiedliche Orte auflisten
                cmd.CommandText = "select distinct(`Location`) as `Location` from " + TableName +
                    " order by `Location` ";

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string location = Convert.ToString(reader["Location"]);                    

                    if (string.IsNullOrEmpty(location) == false)
                    {
                        // zur Ort Liste hinzufügen
                        returnValue.Add(location);
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }
    }
}
