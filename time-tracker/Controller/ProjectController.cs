﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using time_tracker.Models;

namespace time_tracker.Controller
{
    public class ProjectController : BaseController<Project>
    {
        #region Members

        /// <summary>
        /// Projekt Status
        /// </summary>
        public enum ProjectStatus
        {
            Active,
            Inactive,
            All
        }

        #endregion

        /// <summary>
        /// Konstruktor
        /// </summary>
        public ProjectController() : base("`Project`")
        {
        }

        /// <summary>
        /// Liest alle Projekte aus
        /// </summary>
        /// <returns></returns>
        public override List<Project> GetAll()
        {
            return GetAll();
        }

        /// <summary>
        /// Liest alle Projekte aus
        /// </summary>
        /// <param name="status">Projektstatus</param>
        /// <param name="onlyNotSynced">nicht synchronisierte Projekte</param>
        /// <returns>Projekte</returns>
        public List<Project> GetAll(ProjectStatus status = ProjectStatus.Active, bool onlyNotSynced = false)
        {
            List<Project> returnValue = new List<Project>();

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;

                string sql = "select * from " + TableName + " ";
                if (status == ProjectStatus.Active)
                    sql += " where Active = 1";
                else if (status == ProjectStatus.Inactive)
                    sql += " where Active = 0";

                // nur nicht synchronisierte Projekte? 
                if (onlyNotSynced)
                {
                    if (sql.Contains("where") == false)
                        sql += " where ";
                    else
                        sql += " and ";

                    sql += " `Synced` = 0 ";
                }
                else
                {
                    // sonst keine gelöschten Datensätze
                    if (sql.Contains("where") == false)
                        sql += " where ";
                    else
                        sql += " and ";

                    sql += " `Deleted` = 0 ";
                }

                sql += " order by `Name` ";

                cmd.CommandText = sql;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Projekt laden
                    Project project = FieldsToModel(reader);

                    if (project != null)
                    {
                        // zur Projekt Liste hinzufügen
                        returnValue.Add(project);
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Liest alle nicht synchronisierten Projekte aus
        /// </summary>
        /// <returns></returns>
        public override List<Project> GetAllNotSynced()
        {
            return GetAll(ProjectStatus.All, true);
        }

        /// <summary>
        /// Liest das angegebene Projekt aus
        /// </summary>
        /// <param name="id">ID des Projekts</param>
        /// <returns>Projekt</returns>
        public override Project GetSingle(int id)
        {
            Project returnValue = null;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from " + TableName + " where ID = " + id;

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    // Projekt laden
                    returnValue = FieldsToModel(reader);
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <inheritdoc />
        protected override Project FieldsToModel(SQLiteDataReader reader)
        {
            Project returnValue = null;

            try
            {
                returnValue = new Project();

                // Pflichtfelder
                returnValue.ID = Convert.ToInt32(reader["ID"]);
                returnValue.Name = Convert.ToString(reader["Name"]);
                returnValue.Active = Convert.ToInt32(reader["Active"]);
                returnValue.Synced = Convert.ToInt32(reader["Synced"]);
                returnValue.Deleted = Convert.ToInt32(reader["Deleted"]);

                // optionale Felder
                returnValue.Private = Convert.ToInt32(reader["Private"]);

                if (reader["ObjectId"].Equals(DBNull.Value) == false)
                    returnValue.ObjectId = Convert.ToInt32(reader["ObjectId"]);
                else
                    returnValue.ObjectId = -1;
            }
            catch
            {
                return null;
            }

            return returnValue;
        }

        /// <summary>
        /// Speichert das Projekt
        /// </summary>
        /// <param name="row">Projekt</param>
        /// <returns>affected rows oder ID des eingefügten Datensatzes</returns>
        public override int Insert(Project row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into " + TableName + " " +
                    "(`Name`, Active, Private, Synced, Deleted, ObjectId) values " +
                    "(@name, @active, @private, @synced, @deleted, @objectId)";

                // Parameter setzen
                cmd.Parameters.Add("name", DbType.String).Value = row.Name;
                cmd.Parameters.Add("active", DbType.Int32).Value = row.Active;
                cmd.Parameters.Add("private", DbType.Int32).Value = row.Private;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("deleted", DbType.Int32).Value = row.Deleted;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // Es wird eig. keine ID sondern nur eine Reihennummer vergeben. Diese Reihennummer 
                // wird hinzugefügt, falls die Zeit hinzugefügt, gespeichert und wieder gelöscht wird 
                // OHNE die Datenbank neu zu öffnen.
                cmd.CommandText = "select last_insert_rowid()";
                object ID = cmd.ExecuteScalar();

                // ID vergeben
                returnValue = Convert.ToInt32(ID);
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Aktualisiert das Projekt
        /// </summary>
        /// <param name="row">Projekt</param>
        /// <returns>affected rows oder ID des Datensatzes</returns>
        public override int Update(Project row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update " + TableName + " set " +
                    "`Name` = @name, Active = @active, Private = @private, Synced = @synced, ObjectId = @objectId " +
                    "where ID = " + row.ID;

                // Parameter setzen
                cmd.Parameters.Add("name", DbType.String).Value = row.Name;
                cmd.Parameters.Add("active", DbType.Int32).Value = row.Active;
                cmd.Parameters.Add("private", DbType.Int32).Value = row.Private;
                cmd.Parameters.Add("synced", DbType.Int32).Value = row.Synced;
                cmd.Parameters.Add("objectId", DbType.Int32).Value = row.ObjectId;

                cmd.ExecuteNonQuery();

                // ID zurückgeben
                returnValue = row.ID;
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Liest die Gesamtzeit des angegebenen Projekts aus
        /// </summary>
        /// <param name="id">ID des Projekts</param>
        /// <returns>Gesamt Sekunden</returns>
        public static long getTotalProjectTime(int id)
        {
            long returnValue = 0;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from Task where ProjectID = " + id + " and `To` is not NULL";

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    DateTime from = Helper.Sqlite.Timestamp2DateTime(Convert.ToInt32(reader["From"]));
                    DateTime to = Helper.Sqlite.Timestamp2DateTime(Convert.ToInt32(reader["To"]));

                    TimeSpan span = to - from;
                    returnValue += span.Ticks;
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }
    }
}
