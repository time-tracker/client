﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Controller
{
    /// <summary>
    /// Basis Controller
    /// </summary>
    public abstract class BaseController<T> where T : Models.BaseModel
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="tableName">Tabellenname</param>
        public BaseController(string tableName)
        {
            TableName = tableName;
        }

        /// <summary>
        /// Tabellenname
        /// </summary>
        protected string TableName { get; set; }

        /// <summary>
        /// Liste mit allen Datensätzen zurückgeben
        /// </summary>
        /// <returns></returns>
        public virtual List<T> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Liest alle nicht synchronisierten Datensätze aus
        /// </summary>
        /// <returns></returns>
        public virtual List<T> GetAllNotSynced()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Einen Datensatz zurückgeben
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        public virtual T GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Einen Datensatz Einfügen / Speichern
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public virtual int Insert(T row)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Einen Datensatz aktualisieren
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public virtual int Update(T row)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Felder eines Datensatzes in ein Objekt umwandeln
        /// </summary>
        protected virtual T FieldsToModel(SQLiteDataReader reader)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Einen Datensatz Löschen (Löschkennzeichen)
        /// </summary>
        /// <param name="row">Datensatz</param>
        /// <returns></returns>
        public virtual int Delete(T row)
        {
            int returnValue = -1;

            if (row.ObjectId <= 0)
            {
                // wenn der Datensatz noch nicht mit dem Backend sychronisiert wurde, dann direkt löschen
                returnValue = DeletePermanent(row);
            }
            else
            {
                // sonst Löschkennzeichen setzen
                SQLiteConnection db = new SQLiteConnection();
                try
                {
                    db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                    db.Open();

                    SQLiteCommand cmd = new SQLiteCommand();
                    cmd.Connection = db;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "update " + TableName + " set " +
                        " Synced = 0, `Deleted` = 1 " +
                        " where ID = " + row.ID;

                    returnValue = cmd.ExecuteNonQuery();
                }
                finally
                {
                    db.Close();
                    db.Dispose();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Einen Datensatz Löschen
        /// </summary>
        /// <param name="row">Datensatz</param>
        /// <returns></returns>
        public int DeletePermanent(T row)
        {
            int returnValue = -1;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "delete from " + TableName + " where ID = " + row.ID;

                returnValue = cmd.ExecuteNonQuery();
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }
    }
}
