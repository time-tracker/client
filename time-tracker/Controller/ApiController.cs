﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Diagnostics;

namespace time_tracker.Controller
{
    /// <summary>
    /// Api Controller
    /// </summary>
    public class ApiController
    {
        private SettingController settings;
        private EventController eventController;
        private ProjectController projectController;
        private TaskController taskController;
        private RestClient client;
        private Helper.RestSharpJsonNetSerializer serializer;
        private JwtAuthenticator authenticator;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="throwExceptions"></param>
        public ApiController(bool throwExceptions = true)
        {
            // Props
            ThrowExceptions = throwExceptions;

            // Controller
            settings = new SettingController();
            eventController = new EventController();
            projectController = new ProjectController();
            taskController = new TaskController();

            // rest client
            client = new RestClient(settings.getStringParameter(SettingController.API_URL));

            // serializer
            serializer = new Helper.RestSharpJsonNetSerializer();
            serializer.DateFormat = "yyyy-MM-dd HH:mm:ss";
        }

        /// <summary>
        /// Ins Backend (API) eingeloggter User
        /// </summary>
        public static Models.UserResponse ApiUser { get; set; } = null;

        /// <summary>
        /// Sollen die Exceptions weitergeworfen werden?
        /// </summary>
        public bool ThrowExceptions { get; private set; }

        /// <summary>
        /// Backend (API) Sync ausführen
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ApiException"></exception>
        public SyncResponse DoSync()
        {
            ApiUser = DoLogin();

            // war der Login erfolgreich?
            if (ApiUser != null)
            {
                // authentication
                authenticator = new JwtAuthenticator(ApiUser.Token);
                client.Authenticator = authenticator;

                DoEventMigrate();
                DoProjectMigrate();
                DoTaskMigrate();

                return new SyncResponse()
                {
                    SyncedEventsCount = 0,
                    SyncedProjectsCount = 0,
                    SyncedTasksCount = 0
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Login ausführen
        /// </summary>
        /// <returns></returns>
        private Models.UserResponse DoLogin()
        {
            // wenn ApiUser und Token vorhanden, dann gleich zurückgeben
            if (ApiUser != null)
            {
                if (string.IsNullOrEmpty(ApiUser.Token) == false)
                    return ApiUser;
            }

            // sonst Login ausführen
            string apiLoginEmail = settings.getStringParameter(SettingController.API_EMAIL);
            string apiLoginPassword = settings.getStringParameter(SettingController.API_PASSWORD);

            if ((string.IsNullOrEmpty(apiLoginEmail) == false) &&
                (string.IsNullOrEmpty(apiLoginPassword) == false))
            {
                try
                {
                    // create the user
                    Models.User user = new Models.User()
                    {
                        Email = apiLoginEmail,
                        Password = apiLoginPassword
                    };

                    // create the request
                    var loginRequest = new RestRequest("login", Method.POST);
                    loginRequest.JsonSerializer = serializer;
                    loginRequest.AddJsonBody(user);

                    // execute the request
                    IRestResponse<Models.UserResponse> response = client.Execute<Models.UserResponse>(loginRequest);

                    // check the response
                    if (response.ErrorException != null)
                    {
                        throw new ApiException("exception in response: " + response.ErrorMessage);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                    {
                        string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                        throw new ApiException("wrong login information: " + errorMessage);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                        throw new ApiException("interal server error: " + errorMessage);
                    }
                    else
                    {
                        // request successful -> return user response data
                        return response.Data;
                    }
                }
                catch (ApiException apiError)
                {
                    if (ThrowExceptions)
                        throw apiError;
                    else
                    {
                        Debug.WriteLine(apiError.Message);
                        return null;
                    }
                }
                catch (Exception error)
                {
                    Debug.WriteLine(error.Message);
                    return null;
                }
            }
            else
            {
                Debug.WriteLine("login information is not set");
                return null;
            }
        }

        /// <summary>
        /// Events synchronisieren
        /// </summary>
        private void DoEventMigrate()
        {
            // Server zur lokalen Datenbank ( seit dem letzten Sync - Zeitstempel! )

            // Lokale Datenbank zum Server
            var events = eventController.GetAllNotSynced();
            foreach (Models.Event e in events)
            {
                if (e.Synced == 0)
                {
                    // exec sync with RestClient
                    try
                    {
                        // create the request
                        RestRequest eventRequest = null;

                        // was the event synced with backend before?
                        if (e.ObjectId <= 0)
                        {
                            // NO! -> POST request
                            eventRequest = new RestRequest("event", Method.POST);
                        }
                        else
                        {
                            // was the event deleted in local db?
                            if (e.Deleted == 1)
                            {
                                // YES! -> DELETE request
                                eventRequest = new RestRequest("event/" + e.ObjectId, Method.DELETE);
                            }
                            else
                            {
                                // NO! -> PUT request
                                eventRequest = new RestRequest("event/" + e.ObjectId, Method.PUT);
                            }
                        }

                        // authentication
                        client.Authenticator.Authenticate(client, eventRequest);

                        // serialize
                        eventRequest.JsonSerializer = serializer;
                        eventRequest.AddJsonBody(e);

                        // execute the request
                        IRestResponse<Models.EventResponse> response = client.Execute<Models.EventResponse>(eventRequest);

                        // check the response
                        if (response.ErrorException != null)
                        {
                            throw new ApiException("exception in response: " + response.ErrorMessage);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                        {
                            string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                            throw new ApiException("on must be unique: " + errorMessage);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                            throw new ApiException("interal server error: " + errorMessage);
                        }
                        else
                        {
                            // sync successful
                            if (e.Deleted == 1)
                            {
                                // delete event permanetly
                                eventController.DeletePermanent(e);
                            }
                            else
                            {
                                // update event in local db
                                e.Synced = 1;
                                e.ObjectId = response.Data.ID;
                                eventController.Update(e);
                            }
                        }
                    }
                    catch (ApiException apiError)
                    {
                        if (ThrowExceptions)
                            throw apiError;
                        else
                        {
                            Debug.WriteLine(apiError.Message);
                        }
                    }
                    catch (Exception error)
                    {
                        Debug.WriteLine(error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Projekte synchronisieren
        /// </summary>
        private void DoProjectMigrate()
        {
            // Server zur lokalen Datenbank ( seit dem letzten Sync - Zeitstempel! )

            // Lokale Datenbank zum Server
            var projects = projectController.GetAllNotSynced();
            foreach (Models.Project p in projects)
            {
                if (p.Synced == 0)
                {
                    // exec sync with RestClient
                    try
                    {
                        // create the request
                        RestRequest projectRequest = null;

                        // was the project synced with backend before?
                        if (p.ObjectId <= 0)
                        {
                            // NO! -> POST request
                            projectRequest = new RestRequest("project", Method.POST);
                        }
                        else
                        {
                            // was the project deleted in local db?
                            if (p.Deleted == 1)
                            {
                                // YES! -> DELETE request
                                projectRequest = new RestRequest("project/" + p.ObjectId, Method.DELETE);
                            }
                            else
                            {
                                // NO! -> PUT request
                                projectRequest = new RestRequest("project/" + p.ObjectId, Method.PUT);
                            }
                        }

                        // authentication
                        client.Authenticator.Authenticate(client, projectRequest);

                        // serialize
                        projectRequest.JsonSerializer = serializer;
                        projectRequest.AddJsonBody(p);

                        // execute the request
                        IRestResponse<Models.ProjectResponse> response = client.Execute<Models.ProjectResponse>(projectRequest);

                        // check the response
                        if (response.ErrorException != null)
                        {
                            throw new ApiException("exception in response: " + response.ErrorMessage);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                        {
                            string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                            throw new ApiException("on must be unique: " + errorMessage);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                            throw new ApiException("interal server error: " + errorMessage);
                        }
                        else
                        {
                            // sync successful -> update project in local db
                            if (p.Deleted == 1)
                            {
                                // delete project permanetly
                                projectController.DeletePermanent(p);
                            }
                            else
                            {
                                // update project in local db
                                p.Synced = 1;
                                p.ObjectId = response.Data.ID;
                                projectController.Update(p);
                            }
                        }
                    }
                    catch (ApiException apiError)
                    {
                        if (ThrowExceptions)
                            throw apiError;
                        else
                        {
                            Debug.WriteLine(apiError.Message);
                        }
                    }
                    catch (Exception error)
                    {
                        Debug.WriteLine(error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Tasks synchronisieren
        /// </summary>
        private void DoTaskMigrate()
        {
            // Server zur lokalen Datenbank ( seit dem letzten Sync - Zeitstempel! )

            // Lokale Datenbank zum Server
            var tasks = taskController.GetAllNotSynced();
            foreach (Models.Task t in tasks)
            {
                if (t.Synced == 0)
                {
                    // project from the task was not synced before
                    if (t.ProjectID == -1)
                    {
                        // force project to sync next time
                        t.Project.Synced = 0;
                        projectController.Update(t.Project);
                    }
                    else
                    {
                        // exec sync with RestClient
                        try
                        {
                            // create the request
                            RestRequest taskRequest = null;

                            // was the task synced with backend before?
                            if (t.ObjectId <= 0)
                            {
                                // NO! -> POST request
                                taskRequest = new RestRequest("task", Method.POST);
                            }
                            else
                            {
                                if (t.Deleted == 1)
                                {
                                    // YES! -> DELETE request
                                    taskRequest = new RestRequest("task/" + t.ObjectId, Method.DELETE);
                                }
                                else
                                {
                                    // NO! -> PUT request
                                    taskRequest = new RestRequest("task/" + t.ObjectId, Method.PUT);
                                }
                            }

                            // authentication
                            client.Authenticator.Authenticate(client, taskRequest);

                            // serialize
                            taskRequest.JsonSerializer = serializer;
                            taskRequest.AddJsonBody(t);

                            // execute the request
                            IRestResponse<Models.TaskResponse> response = client.Execute<Models.TaskResponse>(taskRequest);

                            // check the response
                            if (response.ErrorException != null)
                            {
                                throw new ApiException("exception in response: " + response.ErrorMessage);
                            }
                            else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                            {
                                string errorMessage = (int)response.StatusCode + " " + response.StatusDescription;
                                throw new ApiException("interal server error: " + errorMessage);
                            }
                            else
                            {
                                // sync successful
                                if (t.Deleted == 1)
                                {
                                    // delete task permanetly
                                    taskController.DeletePermanent(t);
                                }
                                else
                                {
                                    // update task in local db
                                    t.Synced = 1;
                                    t.ObjectId = response.Data.ID;
                                    taskController.Update(t);
                                }
                            }
                        }
                        catch (ApiException apiError)
                        {
                            if (ThrowExceptions)
                                throw apiError;
                            else
                            {
                                Debug.WriteLine(apiError.Message);
                            }
                        }
                        catch (Exception error)
                        {
                            Debug.WriteLine(error.Message);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Klasse für Sync Response
    /// </summary>
    public class SyncResponse
    {
        public int SyncedEventsCount { get; set; }
        public int SyncedProjectsCount { get; set; }
        public int SyncedTasksCount { get; set; }
    }

    /// <summary>
    /// Exception der API
    /// </summary>
    public class ApiException : Exception
    {
        public ApiException(string message) : base("ApiException: " + message)
        {
        }
    }
}
