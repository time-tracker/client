﻿using System;
using System.Data.SQLite;
using System.IO;

namespace time_tracker.Controller
{
    public class DatabaseController
    {
        /// <summary>
        /// Datenbank rundum Check
        /// </summary>
        /// <returns>Ja/Nein</returns>
        public static void checkDatabase()
        {
            // Ist das Datenbank Verzeichnis vorhanden?
            if (Directory.Exists(Global.Param.DB_PATH) == false)
            {
                Directory.CreateDirectory(Global.Param.DB_PATH);
            }
            
            // Ist die Datenbank vorhanden?
            if (File.Exists(Global.Param.DB_FILENAME))
            {
                // Backup erstellen
                backupDatabase();

                // Datenbank öffnen
                SQLiteConnection db = new SQLiteConnection();
                try
                {
                    db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                    db.Open();

                    // aktuelle Datenbankversion auslesen
                    SQLiteCommand cmdVersion = new SQLiteCommand("PRAGMA user_version", db);
                    int currentVersion = Convert.ToInt32(cmdVersion.ExecuteScalar().ToString());

                    // ggf. Update
                    updateDatabase(db, currentVersion);
                }
                catch
                {
                    throw new Exception("Fehler beim Datenbank Update");
                }
                finally
                {
                    db.Close();
                    db.Dispose();
                }
            }
            else
            {
                // neue Datenbank erstellen
                createDatabase();
            }
        }
        
        /// <summary>
        /// neue Datenbank erstellen
        /// </summary>
        private static void createDatabase()
        {
            SQLiteConnection.CreateFile(Global.Param.DB_FILENAME);
            SQLiteConnection dbCreate = new SQLiteConnection();
            try
            {
                dbCreate.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                dbCreate.Open();

                // Datenbank Einstellungen
                string sqlPragma = "PRAGMA user_version = " + Global.Param.DB_VERSION + ";" + Environment.NewLine +
                                   "PRAGMA foreign_keys = true;";

                SQLiteCommand cmdPragma = new SQLiteCommand(sqlPragma, dbCreate);
                cmdPragma.ExecuteNonQuery();

                // Tabelle: Project
                string sqlProject = "CREATE TABLE `Project` ( " +
                                    " `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                                    " `Name` TEXT NOT NULL, " +
                                    " `Active` INTEGER NOT NULL DEFAULT 1, " +
                                    " `Private` INTEGER NULL, " +
                                    " `Synced` INTEGER NOT NULL DEFAULT 0, " +
                                    " `Deleted` INTEGER NOT NULL DEFAULT 0, " +
                                    " `ObjectId` INTEGER NULL " +
                                    " )";

                SQLiteCommand cmdProject = new SQLiteCommand(sqlProject, dbCreate);
                cmdProject.ExecuteNonQuery();

                // Tabelle: Event
                string sqlEvent = "CREATE TABLE `Event` ( " +
                                    " `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                                    " `On` INTEGER NOT NULL UNIQUE, " +
                                    " `Category` INTEGER NOT NULL, " +
                                    " `Note` TEXT NULL, " +
                                    " `Synced` INTEGER NOT NULL DEFAULT 0, " +
                                    " `Deleted` INTEGER NOT NULL DEFAULT 0, " +
                                    " `ObjectId` INTEGER NULL " +
                                    " )";

                SQLiteCommand cmdEvent = new SQLiteCommand(sqlEvent, dbCreate);
                cmdEvent.ExecuteNonQuery();

                // Tabelle: Setting
                string sqlSetting = "CREATE TABLE 'Setting' ( " +
                                    " `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                                    " `Key`	TEXT NOT NULL UNIQUE, " +
                                    " `Value` TEXT NOT NULL " +
                                    " )";

                SQLiteCommand cmdSetting = new SQLiteCommand(sqlSetting, dbCreate);
                cmdSetting.ExecuteNonQuery();

                // Tabelle: Task
                string sqlTask = "CREATE TABLE 'Task' ( " +
                                 " `ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                                 " `From`	INTEGER NOT NULL, " +
                                 " `To`	INTEGER, " +
                                 " `ProjectID` INTEGER, " +
                                 " `Note` TEXT, " +
                                 " `Location` TEXT, " +
                                 " `Booked` INTEGER NULL, " +
                                 " `Synced` INTEGER NOT NULL DEFAULT 0, " +
                                 " `Deleted` INTEGER NOT NULL DEFAULT 0, " +
                                 " `ObjectId` INTEGER NULL, " +
                                 " FOREIGN KEY(`ProjectID`) REFERENCES `Project`(`ID`) " +
                                 " )";

                SQLiteCommand cmdTask = new SQLiteCommand(sqlTask, dbCreate);
                cmdTask.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Fehler bei der Datenbank Erstellung");
            }
            finally
            {
                dbCreate.Close();
                dbCreate.Dispose();
            }
        }

        /// <summary>
        /// Datenbank aktualisieren
        /// </summary>
        /// <param name="db">Datenbank Verbindung</param>
        /// <param name="currentVersion">aktuelle Datenbankversion</param>
        private static void updateDatabase(SQLiteConnection db, int currentVersion)
        {
            if (db.State == System.Data.ConnectionState.Closed)
                db.Open();

            // Unterschiedliche Datenbank Versionen?
            if (currentVersion != Global.Param.DB_VERSION)
            {
                // Update auf Version 2
                if (currentVersion == 1)
                {
                    // neue Spalte Private in Projekt
                    string sqlPrivate = "alter table Project add column Private int null;" +  Environment.NewLine +
                                      "update Project set Private = 0;";
                    SQLiteCommand cmdPrivate = new SQLiteCommand(sqlPrivate, db);
                    cmdPrivate.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 2;
                }
                // Update auf Version 3
                if (currentVersion == 2)
                {
                    // neue Tabelle: Event
                    string sqlEvent = "CREATE TABLE `Event` ( " +
                                        " `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                                        " `On` INTEGER NOT NULL UNIQUE, " +
                                        " `Category` INTEGER NOT NULL, " +
                                        " `Note` TEXT NULL " +
                                        " )";

                    SQLiteCommand cmdEvent = new SQLiteCommand(sqlEvent, db);
                    cmdEvent.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 3;
                }
                // Update auf Version 4
                if (currentVersion == 3)
                {
                    // neue Spalte Booked in Task
                    string sqlBooked = "alter table `Task` add column Booked int null;" + Environment.NewLine +
                                      "update `Task` set Booked = 0;";
                    SQLiteCommand cmdBooked = new SQLiteCommand(sqlBooked, db);
                    cmdBooked.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 4;
                }
                // Update auf Version 5
                if (currentVersion == 4)
                {
                    // neue Spalte Location in Task
                    string sqlLocation = "alter table `Task` add column `Location` TEXT null;";
                    SQLiteCommand cmdLocation = new SQLiteCommand(sqlLocation, db);
                    cmdLocation.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 5;
                }
                // Update auf Version 6
                if (currentVersion == 5)
                {
                    // neue API Spalten in Event
                    string sqlEventApiFields = "alter table `Event` add column `Synced` INTEGER NOT NULL DEFAULT 0;" + Environment.NewLine +
                        "alter table `Event` add column `ObjectId` int null;";
                    SQLiteCommand cmdEventApiFields = new SQLiteCommand(sqlEventApiFields, db);
                    cmdEventApiFields.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 6;
                }
                // Update auf Version 7
                if (currentVersion == 6)
                {
                    // neue API Spalten in Event
                    string sqlProjectApiFields = "alter table `Project` add column `Synced` INTEGER NOT NULL DEFAULT 0;" + Environment.NewLine +
                        "alter table `Project` add column `ObjectId` int null;";
                    SQLiteCommand cmdProjectApiFields = new SQLiteCommand(sqlProjectApiFields, db);
                    cmdProjectApiFields.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 7;
                }
                // Update auf Version 8
                if (currentVersion == 7)
                {
                    // neue API Spalten in Event
                    string sqlTaskApiFields = "alter table `Task` add column `Synced` INTEGER NOT NULL DEFAULT 0;" + Environment.NewLine +
                        "alter table `Task` add column `ObjectId` int null;";
                    SQLiteCommand cmdTaskApiFields = new SQLiteCommand(sqlTaskApiFields, db);
                    cmdTaskApiFields.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 8;
                }
                // Update auf Version 9
                if (currentVersion == 8)
                {
                    // neue API Spalten in allen sychronisierten Tabellen
                    string sqlApiFields = "alter table `Task` add column `Deleted` INTEGER NOT NULL DEFAULT 0;" + Environment.NewLine +
                        "alter table `Project` add column `Deleted` INTEGER NOT NULL DEFAULT 0;" + Environment.NewLine +
                        "alter table `Event` add column `Deleted` INTEGER NOT NULL DEFAULT 0";
                    SQLiteCommand cmdApiFields = new SQLiteCommand(sqlApiFields, db);
                    cmdApiFields.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 9;
                }
                // Update auf Version 10
                if (currentVersion == 9)
                {
                    // Object Id auf -1 setzen
                    string sqlUpdateObjectId = "update `Task` set `ObjectId` = -1 where `ObjectId` is NULL;" + Environment.NewLine +
                        "update `Project` set `ObjectId` = -1 where `ObjectId` is NULL;" + Environment.NewLine +
                        "update `Event` set `ObjectId` = -1 where `ObjectId` is NULL;";
                    SQLiteCommand cmdUpdateObjectId = new SQLiteCommand(sqlUpdateObjectId, db);
                    cmdUpdateObjectId.ExecuteNonQuery();

                    // currentVersion aktualisieren
                    currentVersion = 10;
                }

                // Datenbank Version aktualisieren
                string sqlVersion = "PRAGMA user_version = " + Global.Param.DB_VERSION;
                SQLiteCommand cmdPragma = new SQLiteCommand(sqlVersion, db);
                cmdPragma.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Backup der Datenbank erstellen
        /// </summary>
        private static void backupDatabase()
        {
            // nur im Live Betrieb wird ein Backup erstellt
            if (Global.Param.isDebug == false)
            {
                // Backup erstellen
                try
                {
                    File.Copy(Global.Param.DB_FILENAME, Global.Param.DB_BACKUP_FILENAME, true);
                }
                catch
                {
                    throw new Exception("Fehler beim Erstellen der Datenbank Sicherung");
                }

                // Backups aufräumen
                try
                {
                    // Objekt zum Verzeichnis durchlaufen vorbereiten
                    DirectoryInfo di = new DirectoryInfo(Global.Param.DB_PATH);

                    // alle Backups suchen
                    FileInfo[] fiArray = di.GetFiles("*" + Global.Param.DB_BACKUP_UNIQUE_STR + "*");

                    // Sind mehr Backups wie vorgehalten sollen vorhanden?
                    int i = fiArray.Length - Global.Param.BACKUPS_TO_KEEP;
                    if (i > 0)
                    {
                        // die überschüssigen Backups löschen
                        for (int x = 0; x < i; x++)
                            File.Delete(fiArray[x].FullName);
                    }
                }
                catch
                {
                    throw new Exception("Fehler beim Aufräumen der Datenbank Sicherungen");
                }
            }
        }
    }
}
