﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;


namespace time_tracker.Controller
{
    public class SettingController
    {
        #region Parameter

        public const string LUNCH_TIME = "LUNCH_TIME";
        public const string DAILY_WORKING_TIME = "DAILY_WORKING_TIME";
        public const string OPEN_AT_START = "OPEN_AT_START";
        public const string TASK_SHOW_LOCATION = "TASK_SHOW_LOCATION";
        public const string API_EMAIL = "API_EMAIL";
        public const string API_PASSWORD = "API_PASSWORD";
        public const string API_URL = "API_URL";

        /// <summary>
        /// Standardwerte für die einzelnen Einstellungen.
        /// </summary>
        private Dictionary<string, string> defaultValues = new Dictionary<string, string>()
        {
            { LUNCH_TIME, "1" },
            { DAILY_WORKING_TIME, "8" },
            { OPEN_AT_START, "0" },
            { TASK_SHOW_LOCATION, "1" },
            { API_EMAIL, "" },
            { API_PASSWORD, "" },
            { API_URL, "https://timetracker.io/api/v1" },
        };

        #endregion

        /// <summary>
        /// Konstruktor
        /// </summary>
        public SettingController()
        {
            // foo
        }

        /// <summary>
        /// Einstellung aus der Datenbank laden. Wenn die Einstellung nicht gefunden wurde, gibt die 
        /// Funktion 'null' zurück
        /// </summary>
        /// <param name="parameter"></param>
        private Models.Setting getEinstellung(string parameter)
        {
            Models.Setting returnValue = null;

            SQLiteConnection db = new SQLiteConnection();
            try
            {
                db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                db.Open();

                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = db;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from Setting where `Key` = '" + parameter + "'";

                SQLiteDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        returnValue = new Models.Setting();
                        
                        // Pflichtfelder
                        returnValue.ID = Convert.ToInt32(reader["ID"]);
                        returnValue.Key = Convert.ToString(reader["Key"]);
                        returnValue.Value = Convert.ToString(reader["Value"]);
                    }
                }
                else
                {
                    // Reader schließen
                    reader.Close();

                    // Ist die Einstellungen in den Standardwerten vorhanden?
                    string wert = string.Empty;
                    if (defaultValues.TryGetValue(parameter, out wert))
                    {
                        // Einstellung wurde in den Standardwerten gefunden => anlegen
                        Models.Setting einstellung = new Models.Setting();
                        einstellung.Key = parameter;
                        einstellung.Value = wert;

                        cmd.CommandText = "insert into Setting " +
                            "(`Key`, `Value`) values " +
                            "(@key, @value)";

                        // Parameter setzen
                        cmd.Parameters.Add("key", DbType.String).Value = einstellung.Key;
                        cmd.Parameters.Add("value", DbType.String).Value = einstellung.Value;

                        cmd.ExecuteNonQuery();

                        // Einstellung zurückgeben
                        returnValue = einstellung;
                    }
                    else
                    {
                        // Einstellung wurde in den Standardwerten nicht gefunden.
                        throw new KeyNotFoundException("Die Einstellung << " + parameter + " >> wurde nicht gefunden");
                    }
                }
            }
            finally
            {
                db.Close();
                db.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// Einstellung in Datenbank speichern
        /// </summary>
        /// <param name="parameter"></param>
        private void setEinstellung(string parameter, string stringWert)
        {
            Models.Setting einstellung = getEinstellung(parameter);
            if (einstellung != null)
            {
                einstellung.Value = stringWert;

                SQLiteConnection db = new SQLiteConnection();
                try
                {
                    db.ConnectionString = Global.Param.DB_CONNECTION_STRING;
                    db.Open();

                    SQLiteCommand cmd = new SQLiteCommand();
                    cmd.Connection = db;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "update Setting set " +
                        "Value = @value " +
                        "where `Key` = '" + einstellung.Key + "'";

                    // Parameter setzen
                    cmd.Parameters.Add("value", DbType.String).Value = einstellung.Value;

                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    db.Close();
                    db.Dispose();
                }
            }
        }

        /// <summary>
        /// Einstellung in einen String umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public string getStringParameter(string parameter)
        {
            Models.Setting einstellung = getEinstellung(parameter);

            if (einstellung != null)
                return einstellung.Value;
            else
                return string.Empty;
        }

        /// <summary>
        /// Einstellung in einen Integer umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int getIntParameter(string parameter)
        {
            Models.Setting einstellung = getEinstellung(parameter);

            if (einstellung != null)
                return Convert.ToInt32(einstellung.Value);
            else
                return -1;
        }

        /// <summary>
        /// Einstellungen in einen Boolean umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool getBoolParameter(string parameter)
        {
            Models.Setting einstellung = getEinstellung(parameter);

            if (einstellung != null)
            {
                try
                {
                    //String kann nicht direkt in Bool umgewandelt werden, deshalb zuerst in INT konvertiert
                    int ConvertInt = Convert.ToInt32(einstellung.Value);

                    return Convert.ToBoolean(ConvertInt);
                }
                catch
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Wert der Einstellung in einen Boolean umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="wert"></param>
        /// <returns></returns>
        public bool setBoolParameter(string parameter, bool wert)
        {
            string stringWert = "";

            // der Wert wird in einen String umgewandelt
            if (wert == true)
                stringWert = "1";
            else
                stringWert = "0";

            setEinstellung(parameter, stringWert);

            return wert;
        }

        /// <summary>
        /// Wert der Einstellung in einen String umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="wert"></param>
        /// <returns></returns>
        public string setStringParameter(string parameter, string wert)
        {
            setEinstellung(parameter, wert);

            return wert;
        }

        /// <summary>
        /// Wert der Einstellung in einen Integer umwandeln
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="wert"></param>
        /// <returns></returns>
        public int setIntParameter(string parameter, int wert)
        {
            string stringWert = "";

            // der Wert wird in einen Int umgewandelt
            stringWert = Convert.ToString(wert);

            setEinstellung(parameter, stringWert);

            return wert;
        }
    }
}



        