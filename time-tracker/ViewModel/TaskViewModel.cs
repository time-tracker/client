﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker.ViewModel
{
    public class TaskViewModel : DialogFrameViewModel
    {
        #region Member

        private TaskController taskController = null;
        private Models.Task taskObj = null;
        private ProjectController projectController = null;
        private DateTime _fromDate = DateTime.Now.Date;
        private DateTime _toDate = DateTime.Now.Date;
        private List<string> fromHourList = null;
        private List<string> fromMinuteList = null;
        private List<string> toHourList = null;
        private List<string> toMinuteList = null;
        private string _selectedFromHour = string.Empty;
        private string _selectedFromMinute = string.Empty;
        private string _selectedToHour = string.Empty;
        private string _selectedToMinute = string.Empty;

        #endregion


        #region Init & Close

        /// <summary>
        /// Konstruktor
        /// </summary>
        public TaskViewModel()
        {
            taskController = new TaskController();
            projectController = new ProjectController();
            taskObj = new Models.Task();

            // Vorbelegungen
            fromHourList = Helper.TimeCalc.getHourList(true);
            fromMinuteList = Helper.TimeCalc.getMinuteList();
            toHourList = Helper.TimeCalc.getHourList(true);
            toMinuteList = Helper.TimeCalc.getMinuteList();
        }

        public override void Cleanup()
        {
            taskObj.Init();
            base.Cleanup();
        }

        /// <summary>
        /// Datensatz mit der angegebenen ID aus der Datenbank lesen
        /// </summary>
        /// <param name="id">Datensatz ID</param>
        protected override void LoadDataRow(int id)
        {
            taskObj = taskController.GetSingle(id);

            if (IsCopy)
            {
                taskObj.ID = 0;
                taskObj.Synced = 0;
                taskObj.ObjectId = -1;
            }

            SetDateAndHourFields();

            base.LoadDataRow(id);
        }

        private void SetDateAndHourFields()
        {
            if (taskObj.From == DateTime.MinValue)
            {
                _fromDate = DateTime.Now.Date;
                _selectedFromHour = DateTime.Now.ToString("HH");
                _selectedFromMinute = DateTime.Now.ToString("mm");
            }
            else
            {
                _fromDate = taskObj.From.Date;
                _selectedFromHour = taskObj.From.ToString("HH");
                _selectedFromMinute = taskObj.From.ToString("mm");
            }
            calcFromDateTime();

            if (taskObj.To == DateTime.MinValue)
            {
                _toDate = DateTime.Now.Date;
                _selectedToHour = DateTime.Now.ToString("HH");
                _selectedToMinute = DateTime.Now.ToString("mm");
            }
            else
            {
                _toDate = taskObj.To.Date;
                _selectedToHour = taskObj.To.ToString("HH");
                _selectedToMinute = taskObj.To.ToString("mm");
            }
            calcToDateTime();
        }

        #endregion


        #region Properties

        /// <summary>
        /// Task ID
        /// </summary>
        public override int Id
        {
            get { return taskObj.ID; }
            set
            {
                if (value > 0)
                {
                    LoadDataRow(value);
                }
                else
                {
                    taskObj.ID = value;
                    SetDateAndHourFields();
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Datum Von
        /// </summary>
        public DateTime FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                RaisePropertyChanged(nameof(FromDate));
                calcFromDateTime();
            }
        }

        /// <summary>
        /// Liste mit den Stunden Von
        /// </summary>
        public List<string> FromHourList
        {
            get
            {
                if (string.IsNullOrEmpty(_selectedFromHour))
                    _selectedFromHour = fromHourList.First();

                return fromHourList;
            }
        }

        /// <summary>
        /// selektierte Stunde Von
        /// </summary>
        public string SelectedFromHour
        {
            get { return _selectedFromHour; }
            set
            {
                _selectedFromHour = value;
                RaisePropertyChanged(nameof(SelectedFromHour));
                calcFromDateTime();
            }
        }

        /// <summary>
        /// Liste mit Minuten Von
        /// </summary>
        public List<string> FromMinuteList
        {
            get
            {
                if (string.IsNullOrEmpty(_selectedFromMinute))
                    _selectedFromMinute = fromMinuteList.First();

                return fromMinuteList;
            }
        }

        /// <summary>
        /// selektierte Minute Von
        /// </summary>
        public string SelectedFromMinute
        {
            get { return _selectedFromMinute; }
            set
            {
                _selectedFromMinute = value;
                RaisePropertyChanged(nameof(SelectedFromMinute));
                calcFromDateTime();
            }
        }

        /// <summary>
        /// Von Datum und Zeit zusammensetzen
        /// </summary>
        private void calcFromDateTime()
        {
            taskObj.From = _fromDate.Date.AddHours(Convert.ToDouble(_selectedFromHour)).AddMinutes(Convert.ToDouble(_selectedFromMinute));
        }

        /// <summary>
        /// Datum Bis
        /// </summary>
        public DateTime ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                RaisePropertyChanged(nameof(ToDate));
                calcToDateTime();
            }
        }


        /// <summary>
        /// Liste mit den Stunden Bis
        /// </summary>
        public List<string> ToHourList
        {
            get
            {
                if (string.IsNullOrEmpty(_selectedToHour))
                    _selectedToHour = toHourList.First();

                return toHourList;
            }
        }

        /// <summary>
        /// selektierte Stunde Bis
        /// </summary>
        public string SelectedToHour
        {
            get { return _selectedToHour; }
            set
            {
                _selectedToHour = value;
                RaisePropertyChanged(nameof(SelectedToHour));
                calcToDateTime();
            }
        }

        /// <summary>
        /// Liste mit Minuten Bis
        /// </summary>
        public List<string> ToMinuteList
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedToMinute))
                    SelectedToMinute = toMinuteList.First();

                return toMinuteList;
            }
        }

        /// <summary>
        /// selektierte Minute Bis
        /// </summary>
        public string SelectedToMinute
        {
            get { return _selectedToMinute; }
            set
            {
                _selectedToMinute = value;
                RaisePropertyChanged(nameof(SelectedToMinute));
                calcToDateTime();
            }
        }

        /// <summary>
        /// bis Datum und Zeit zusammensetzen
        /// </summary>
        private void calcToDateTime()
        {
            taskObj.To = _toDate.Date.AddHours(Convert.ToDouble(_selectedToHour)).AddMinutes(Convert.ToDouble(_selectedToMinute));
        }

        /// <summary>
        /// Liste mit Projekten
        /// </summary>
        public List<Models.Project> Projects
        {
            get
            {
                // alle Projekte holen
                List<Models.Project> _Projects = projectController.GetAll();

                // Workaround: wenn bereits ein Projekt zugewiesen ist, dann aus der Projektliste nochmals zuweisen.
                //             Ansonsten funktioniert die Projekt Selektion nicht.
                if (Project != null)
                    Project = _Projects.Find(p => p.ID == taskObj.Project.ID);

                return _Projects;
            }
        }

        /// <summary>
        /// Projekt
        /// </summary>
        public Models.Project Project
        {
            get
            {
                return taskObj.Project;
            }
            set
            {
                taskObj.Project = value;
                RaisePropertyChanged(nameof(Project));
            }
        }

        /// <summary>
        /// Notiz, Bemerkung
        /// </summary>
        public string Note
        {
            get { return taskObj.Note; }
            set
            {
                taskObj.Note = value;
                RaisePropertyChanged(nameof(Note));
            }
        }

        /// <summary>
        /// Zeit Gebucht / Abgerechnet
        /// </summary>
        public int Booked
        {
            get { return taskObj.Booked; }
            set
            {
                taskObj.Booked = value;
                RaisePropertyChanged(nameof(Booked));
            }
        }

        /// <summary>
        /// Ist die Zeit gebucht / abgerechnet?
        /// </summary>
        public bool IsBooked
        {
            get { return Booked == 1; }
            set
            {
                if (value)
                    Booked = 1;
                else
                    Booked = 0;

                RaisePropertyChanged(nameof(Booked));
            }
        }

        /// <summary>
        /// Liste mit Orten
        /// </summary>
        public List<string> Locations
        {
            get
            {
                // alle Orte holen
                return taskController.GetLocations();
            }
        }

        /// <summary>
        /// Ort
        /// </summary>
        public string Location
        {
            get { return taskObj.Location; }
            set
            {
                taskObj.Location = value;
                RaisePropertyChanged(nameof(Location));
            }
        }

        #endregion


        #region Logik

        /// <summary>
        /// Daten speichern
        /// </summary>
        protected override void DialogSaveClick()
        {
            UiServices.SetBusyState();

            if (taskObj.ID == 0)
            {
                // insert
                int x = taskController.Insert(taskObj);
            }
            else
            {
                // update
                taskObj.Synced = 0;
                int x = taskController.Update(taskObj);
            }

            MessengerInstance.Send(new TasksEditedMessage());
            base.DialogSaveClick();
        }

        /// <summary>
        /// Kann Speichern ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected override bool CanDialogSaveClick()
        {
            bool returnValue = true;

            // Validation fixe Parameter
            // Von kann nicht größer sein als Bis
            if (taskObj.From > taskObj.To)
                returnValue = false;

            return returnValue;
        }

        #endregion
    }
}
