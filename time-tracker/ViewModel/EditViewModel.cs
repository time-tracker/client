﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker.ViewModel
{
    public class EditViewModel : ViewModelBase
    {
        #region Member

        private SettingController settings = null;
        private DateTime _tasksFrom;
        private DateTime _tasksTo;
        private TaskController taskController = null;
        private Models.Task _SelectedTask = null;
        private Models.Project _SelectedProject = null;
        private bool _onlyProjectTimes = false;
        private bool _onlyNotBookedTimes = false;
        private ProjectController projectController = null;
        private EventController eventController = null;
        private Models.Event _SelectedEvent = null;
        private StatsController statsController = null;
        private List<string> _StatsGroups = null;
        private int _SelectedStatsGroup = 0;
        private Models.Project _FilterProject = null;
        private string _FilterLocation = string.Empty;

        #endregion

        /// <summary>
        /// Konstruktor
        /// </summary>
        public EditViewModel()
        {
            // Controller
            settings = new SettingController();
            taskController = new TaskController();
            projectController = new ProjectController();
            eventController = new EventController();
            statsController = new StatsController();

            // Commands
            OkCommand = new RelayCommand(() => OkClick());
            NewTaskCommand = new RelayCommand(() => NewButtonClick(Pages.PageRepository.Page.Task));
            NewProjectCommand = new RelayCommand(() => NewButtonClick(Pages.PageRepository.Page.Project));
            NewEventCommand = new RelayCommand(() => NewButtonClick(Pages.PageRepository.Page.Event));
            EditTaskCommand = new RelayCommand(() => EditButtonClick(Pages.PageRepository.Page.Task));
            EditProjectCommand = new RelayCommand(() => EditButtonClick(Pages.PageRepository.Page.Project));
            EditEventCommand = new RelayCommand(() => EditButtonClick(Pages.PageRepository.Page.Event));
            CopyTaskCommand = new RelayCommand(() => CopyButtonClick(Pages.PageRepository.Page.Task));
            CopyProjectCommand = new RelayCommand(() => CopyButtonClick(Pages.PageRepository.Page.Project));
            CopyEventCommand = new RelayCommand(() => CopyButtonClick(Pages.PageRepository.Page.Event));
            TaskBookedCellEditEndingCommand = new RelayCommand<DataGridCellEditEndingEventArgs>(args => TaskEditBookedValue());
            DivideTaskCommand = new RelayCommand(() => DivideTaskButtonClick());
            RegisterUserCommand = new RelayCommand(() => RegisterUser());
            SyncDataCommand = new RelayCommand(() => SyncData());

            // Messages
            Messenger.Default.Register<TasksEditedMessage>(this, m =>
            {
                RefreshTasks();
            });
            Messenger.Default.Register<ProjectsEditedMessage>(this, m =>
            {
                RefreshProjects();
            });
            Messenger.Default.Register<EventsEditedMessage>(this, m =>
            {
                RefreshEvents();
            });

            // Defaults
            _tasksFrom = DateTime.Now.AddMonths(-1);
            _tasksTo = DateTime.Now;

            _StatsGroups = Global.Param.getStatsGroups();
            _SelectedStatsGroup = Global.Param.WEEK_STATS_GROUP;

            settings.setStringParameter(SettingController.API_URL, "https://sebho.uber.space/tt/v1");
        }


        #region Properties

        /// <summary>
        /// Titel des Fensters
        /// </summary>
        public string WindowTitle
        {
            get { return Global.Param.APP_NAME + " - Version " + Helper.AssemblyInfo.AssemblyVersionPrettyPrint; }
        }

        /// <summary>
        /// Zeiten Von
        /// </summary>
        public DateTime TasksFrom
        {
            get { return _tasksFrom; }
            set
            {
                _tasksFrom = value;
                RaisePropertyChanged(nameof(TasksFrom));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Zeiten Bis
        /// </summary>
        public DateTime TasksTo
        {
            get { return _tasksTo; }
            set
            {
                _tasksTo = value;
                RaisePropertyChanged(nameof(TasksTo));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Liste mit Zeiten
        /// </summary>
        public Models.TaskCollection Tasks
        {
            get
            {
                UiServices.SetBusyState();

                // alle Zeiten holen
                Models.TaskCollection tasks = taskController.GetAll(_tasksFrom, _tasksTo, _onlyProjectTimes, _onlyNotBookedTimes,
                    _FilterProject, _FilterLocation);

                // erste Zeit selektieren
                if (tasks.Count > 0)
                    SelectedTask = tasks.First();

                return tasks;
            }
        }

        /// <summary>
        /// Selektierte Zeit
        /// </summary>
        public Models.Task SelectedTask
        {
            get { return _SelectedTask; }
            set
            {
                _SelectedTask = value;
                RaisePropertyChanged(nameof(SelectedTask));
                RaisePropertyChanged(nameof(IsTaskSelected));
            }
        }

        /// <summary>
        /// Ist eine Zeit selektiert?
        /// </summary>
        public bool IsTaskSelected
        {
            get
            {
                if (_SelectedTask == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Filter: nur Projekt Zeiten
        /// </summary>
        public bool Filter_OnlyProjectTimes
        {
            get { return _onlyProjectTimes; }
            set
            {
                _onlyProjectTimes = value;
                RaisePropertyChanged(nameof(Filter_OnlyProjectTimes));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Filter: nur Gebuchte / Abgerechnete Zeiten
        /// </summary>
        public bool Filter_OnlyNotBookedTimes
        {
            get { return _onlyNotBookedTimes; }
            set
            {
                _onlyNotBookedTimes = value;
                RaisePropertyChanged(nameof(Filter_OnlyNotBookedTimes));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Liste mit Projekten für den Filter
        /// </summary>
        public List<Models.Project> Filter_Projects
        {
            get
            {
                // alle Projekte holen
                List<Models.Project> projects = projectController.GetAll(ProjectController.ProjectStatus.Active);

                // Dummy Projekt einfügen
                projects.Insert(0, new Models.ProjectEmtpy());

                // erstes Projekt selektieren
                if (projects.Count > 0)
                    Filter_Project = projects.First();

                return projects;
            }
        }

        /// <summary>
        /// Selektiertes Projekt für Filter
        /// </summary>
        public Models.Project Filter_Project
        {
            get { return _FilterProject; }
            set
            {
                _FilterProject = value;
                RaisePropertyChanged(nameof(Filter_Project));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Liste mit Orten für den Filter
        /// </summary>
        public List<string> Filter_Locations
        {
            get
            {
                // alle Orte holen
                var locations = taskController.GetLocations();

                locations.Insert(0, Global.Param.FILTER_ALL_CAPTION);

                // ersten Ort selektieren
                if (locations.Count > 0)
                    Filter_Location = locations.First();

                return locations;
            }
        }

        /// <summary>
        /// Selektiertes Ort für den Filter
        /// </summary>
        public string Filter_Location
        {
            get { return _FilterLocation; }
            set
            {
                _FilterLocation = value;
                RaisePropertyChanged(nameof(Filter_Location));
                RaisePropertyChanged(nameof(Tasks));
            }
        }

        /// <summary>
        /// Liste mit Projekten
        /// </summary>
        public List<Models.Project> Projects
        {
            get
            {
                UiServices.SetBusyState();

                // alle Projekte holen
                List<Models.Project> projects = projectController.GetAll(ProjectController.ProjectStatus.All);

                // erstes Projekt selektieren
                if (projects.Count > 0)
                    SelectedProject = projects.First();

                return projects;
            }
        }

        /// <summary>
        /// Selektiertes Projekt
        /// </summary>
        public Models.Project SelectedProject
        {
            get { return _SelectedProject; }
            set
            {
                _SelectedProject = value;
                RaisePropertyChanged(nameof(SelectedProject));
                RaisePropertyChanged(nameof(IsProjectSelected));
            }
        }

        /// <summary>
        /// Ist ein Projekt selektiert?
        /// </summary>
        public bool IsProjectSelected
        {
            get
            {
                if (_SelectedProject == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Liste mit Ereignissen
        /// </summary>
        public List<Models.Event> Events
        {
            get
            {
                UiServices.SetBusyState();

                // alle Projekte holen
                List<Models.Event> events = eventController.GetAll();

                // erstes Ereignis selektieren
                if (events.Count > 0)
                    SelectedEvent = events.First();

                return events;
            }
        }

        /// <summary>
        /// Selektiertes Ereignis
        /// </summary>
        public Models.Event SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                _SelectedEvent = value;
                RaisePropertyChanged(nameof(SelectedEvent));
            }
        }

        /// <summary>
        /// Ist ein Event selektiert?
        /// </summary>
        public bool IsEventSelected
        {
            get
            {
                if (_SelectedEvent == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Statistiken Von
        /// </summary>
        public DateTime StatsFrom
        {
            get { return statsController.From; }
            set
            {
                statsController.From = value;
                RaisePropertyChanged(nameof(StatsFrom));
                RaisePropertyChanged(nameof(Stats));
            }
        }

        /// <summary>
        /// Statistiken Bis
        /// </summary>
        public DateTime StatsTo
        {
            get { return statsController.To; }
            set
            {
                statsController.To = value;
                RaisePropertyChanged(nameof(StatsTo));
                RaisePropertyChanged(nameof(Stats));
            }
        }

        /// <summary>
        /// Statistik Gruppierung
        /// </summary>
        public List<string> StatsGroups
        {
            get { return _StatsGroups; }
        }

        /// <summary>
        /// Selektierte Statistik Gruppe
        /// </summary>
        public int SelectedStatsGroup
        {
            get { return _SelectedStatsGroup; }
            set
            {
                _SelectedStatsGroup = value;
                RaisePropertyChanged(nameof(SelectedStatsGroup));
                RaisePropertyChanged(nameof(SelectedStatsGroupStr));
                RaisePropertyChanged(nameof(Stats));
            }
        }

        /// <summary>
        /// Text der selektierten Statistik Gruppe
        /// </summary>
        public string SelectedStatsGroupStr
        {
            get { return _StatsGroups.ElementAt(_SelectedStatsGroup); }
        }

        /// <summary>
        /// Liste mit Statistiken
        /// </summary>
        public Models.StatCollection Stats
        {
            get
            {
                Models.StatCollection returnValue = null;

                switch (SelectedStatsGroup)
                {
                    case 0:
                        returnValue = statsController.DailyView;
                        break;
                    case 1:
                        returnValue = statsController.WeeklyView;
                        break;
                    case 2:
                        returnValue = statsController.MonthlyView;
                        break;
                }

                return returnValue;
            }
        }

        /// <summary>
        /// Setting: Mittagspause in Std.
        /// </summary>
        public int Lunchtime
        {
            get
            {
                return settings.getIntParameter(SettingController.LUNCH_TIME);
            }
            set
            {
                settings.setIntParameter(SettingController.LUNCH_TIME, value);
                RaisePropertyChanged(nameof(Lunchtime));
            }
        }

        /// <summary>
        /// Setting: Arbeitsstunden pro Tag
        /// </summary>
        public int DailyWorkingTime
        {
            get
            {
                return settings.getIntParameter(SettingController.DAILY_WORKING_TIME);
            }
            set
            {
                settings.setIntParameter(SettingController.DAILY_WORKING_TIME, value);
                RaisePropertyChanged(nameof(DailyWorkingTime));
            }
        }

        /// <summary>
        /// Setting: Beim Start anzeigen
        /// </summary>
        public bool OpenAtStart
        {
            get
            {
                return settings.getBoolParameter(SettingController.OPEN_AT_START);
            }
            set
            {
                settings.setBoolParameter(SettingController.OPEN_AT_START, value);
                RaisePropertyChanged(nameof(OpenAtStart));
            }
        }

        /// <summary>
        /// Setting: Beim Start anzeigen
        /// </summary>
        public bool TaskShowLocation
        {
            get
            {
                return settings.getBoolParameter(SettingController.TASK_SHOW_LOCATION);
            }
            set
            {
                settings.setBoolParameter(SettingController.TASK_SHOW_LOCATION, value);
                RaisePropertyChanged(nameof(TaskShowLocation));
            }
        }

        /// <summary>
        /// Setting: API Login Email
        /// </summary>
        public string ApiLoginEmail
        {
            get
            {
                return settings.getStringParameter(SettingController.API_EMAIL);
            }
            set
            {
                settings.setStringParameter(SettingController.API_EMAIL, value);
                RaisePropertyChanged(nameof(ApiLoginEmail));
            }
        }

        /// <summary>
        /// Setting: API Login Password
        /// </summary>
        public string ApiLoginPassword
        {
            get
            {
                return settings.getStringParameter(SettingController.API_PASSWORD);
            }
            set
            {
                settings.setStringParameter(SettingController.API_PASSWORD, value);
                RaisePropertyChanged(nameof(ApiLoginPassword));
            }
        }

        #endregion


        #region Commands

        public ICommand OkCommand { get; private set; }
        public ICommand NewTaskCommand { get; private set; }
        public ICommand NewProjectCommand { get; private set; }
        public ICommand NewEventCommand { get; private set; }
        public ICommand EditTaskCommand { get; private set; }
        public ICommand EditProjectCommand { get; private set; }
        public ICommand EditEventCommand { get; private set; }
        public ICommand CopyTaskCommand { get; private set; }
        public ICommand CopyProjectCommand { get; private set; }
        public ICommand CopyEventCommand { get; private set; }
        public RelayCommand<DataGridCellEditEndingEventArgs> TaskBookedCellEditEndingCommand { get; private set; }
        public ICommand DivideTaskCommand { get; private set; }
        public ICommand RegisterUserCommand { get; private set; }
        public ICommand SyncDataCommand { get; private set; }
        private Base.RelayCommand _deleteProjectCommand = null;
        private Base.RelayCommand _deleteTaskCommand = null;
        private Base.RelayCommand _deleteEventCommand = null;

        /// <summary>
        /// Command zum Projekt löschen Klick
        /// </summary>
        public ICommand DeleteProjectCommand
        {
            get
            {
                if (_deleteProjectCommand == null)
                {
                    _deleteProjectCommand = new Base.RelayCommand(p => this.deleteProject(), p => this.canDeleteProject());
                }

                return _deleteProjectCommand;
            }
        }

        /// <summary>
        /// Command zum Zeit löschen Klick
        /// </summary>
        public ICommand DeleteTaskCommand
        {
            get
            {
                if (_deleteTaskCommand == null)
                {
                    _deleteTaskCommand = new Base.RelayCommand(p => this.deleteTask(), p => this.canDeleteTask());
                }

                return _deleteTaskCommand;
            }
        }

        /// <summary>
        /// Command zum Ereignis löschen Klick
        /// </summary>
        public ICommand DeleteEventCommand
        {
            get
            {
                if (_deleteEventCommand == null)
                {
                    _deleteEventCommand = new Base.RelayCommand(p => this.deleteEvent(), p => this.canDeleteEvent());
                }

                return _deleteEventCommand;
            }
        }

        #endregion


        #region Logik

        /// <summary>
        /// OK Klick
        /// </summary>
        private void OkClick()
        {
            MessengerInstance.Send(new EditWindowCloseMessage());
        }

        /// <summary>
        /// Neu Button Klick
        /// </summary>
        private void NewButtonClick(Pages.PageRepository.Page page)
        {
            // Dialog Window öffnen
            MessengerInstance.Send(new DialogWindowOpenMessage(
                new DialogWindowOpenOptions() {
                    Page = page
                }));
        }

        /// <summary>
        /// Bearbeiten Button Klick
        /// </summary>
        private void EditButtonClick(Pages.PageRepository.Page page)
        {
            // Dialog Window öffnen
            MessengerInstance.Send(new DialogWindowOpenMessage(
                new DialogWindowOpenOptions()
                {
                    Page = page,
                    Id = GetSelectedId(page),
                }));
        }

        /// <summary>
        /// Kopieren Button Klick
        /// </summary>
        private void CopyButtonClick(Pages.PageRepository.Page page)
        {
            // Dialog Window öffnen
            MessengerInstance.Send(new DialogWindowOpenMessage(
                new DialogWindowOpenOptions()
                {
                    Page = page,
                    Id = GetSelectedId(page),
                    IsCopy = true
                }));
        }

        /// <summary>
        /// Zeit teilen Button Klick
        /// </summary>
        private void DivideTaskButtonClick()
        {
            // Dialog Window öffnen
            MessengerInstance.Send(new DialogWindowOpenMessage(
                new DialogWindowOpenOptions()
                {
                    Page = Pages.PageRepository.Page.TaskDivide,
                    Id = GetSelectedId(Pages.PageRepository.Page.TaskDivide),
                }));
        }

        /// <summary>
        /// Datensatz ID anhand der zu öffnenden Page auslesen
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        private int GetSelectedId(Pages.PageRepository.Page page)
        {
            int id = -1;

            switch (page)
            {
                case Pages.PageRepository.Page.Task:
                    id = SelectedTask.ID;
                    break;
                case Pages.PageRepository.Page.Project:
                    id = SelectedProject.ID;
                    break;
                case Pages.PageRepository.Page.Event:
                    id = SelectedEvent.ID;
                    break;
                case Pages.PageRepository.Page.TaskDivide:
                    id = SelectedTask.ID;
                    break;
                default:
                    break;
            }

            return id;
        }

        /// <summary>
        /// Projekt löschen Klick
        /// </summary>
        private void deleteProject()
        {
            projectController.Delete(_SelectedProject);
            RefreshProjects();
        }

        /// <summary>
        /// Projekt löschen Klick erlaubt?
        /// </summary>
        /// <returns></returns>
        private bool canDeleteProject()
        {
            if (_SelectedProject != null)
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Zeit löschen Klick
        /// </summary>
        private void deleteTask()
        {
            taskController.Delete(_SelectedTask);
            RefreshTasks();
        }

        /// <summary>
        /// Zeit löschen Klick erlaubt?
        /// </summary>
        /// <returns></returns>
        private bool canDeleteTask()
        {
            if (_SelectedTask != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Ereignis löschen Klick
        /// </summary>
        private void deleteEvent()
        {
            eventController.Delete(_SelectedEvent);
            RefreshEvents();
        }

        /// <summary>
        /// Ereignis löschen Klick erlaubt?
        /// </summary>
        /// <returns></returns>
        private bool canDeleteEvent()
        {
            if (_SelectedEvent != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gebucht / Abgerechnet Spalte direkt in der Tabelle editiert
        /// </summary>
        private void TaskEditBookedValue()
        {
            if (SelectedTask != null)
            {
                if (SelectedTask.Booked == 1)
                    SelectedTask.Booked = 0;
                else
                    SelectedTask.Booked = 1;

                try
                {
                    taskController.Update(SelectedTask);
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }                
            }
        }

        /// <summary>
        /// Zeiten aktualisieren
        /// </summary>
        private void RefreshTasks()
        {
            UiServices.SetBusyState();

            // Zeiten
            RaisePropertyChanged(nameof(Tasks));
            // Orte aktualisieren
            RaisePropertyChanged(nameof(Filter_Locations));
            // Projekte aktualisieren wegen Gesamt Zeit
            RaisePropertyChanged(nameof(Projects));
            // Statistiken aktualisieren
            StatsFrom = StatsFrom;
        }

        /// <summary>
        /// Projecte aktualisieren
        /// </summary>
        private void RefreshProjects()
        {
            UiServices.SetBusyState();

            // Projekte aktualisieren
            RaisePropertyChanged(nameof(Projects));
        }

        /// <summary>
        /// Ereignisse aktualisieren
        /// </summary>
        private void RefreshEvents()
        {
            UiServices.SetBusyState();

            // Ereignisse
            RaisePropertyChanged(nameof(Events));
            // Statistiken aktualisieren
            StatsFrom = StatsFrom;
        }

        /// <summary>
        /// Im Backend (API) registrieren
        /// </summary>
        private void RegisterUser()
        {
            // register a user
            Console.WriteLine(nameof(RegisterUser));
        }

        /// <summary>
        /// Daten mit Backend (API) abgleichen
        /// </summary>
        private void SyncData()
        {
            Console.WriteLine(nameof(SyncData));
            UiServices.SetBusyState();

            ApiController apiController = new ApiController(false);
            try
            {
                apiController.DoSync();
            }
            catch (ApiException error)
            {
                // throw Backend Error
                MessengerInstance.Send(new BackendError(error.Message));
            }
        }

        #endregion
    }
}
