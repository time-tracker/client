﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace time_tracker.ViewModel
{
    /// <summary>
    /// Basis Klasse für Dialog Frame ViewModels
    /// </summary>
    public abstract class DialogFrameViewModel : ViewModelBase
    {
        #region Init & Close

        /// <summary>
        /// Konstruktor
        /// </summary>
        public DialogFrameViewModel()
        {
            DialogSaveCommand = new RelayCommand(() => DialogSaveClick(), () => CanDialogSaveClick());
            DialogCancelCommand = new RelayCommand(() => DialogCancelClick());
            DialogOkCommand = new RelayCommand(() => DialogOkClick(), () => CanDialogOkClick());
        }

        /// <summary>
        /// Datensatz mit der angegebenen ID aus der Datenbank lesen
        /// </summary>
        /// <param name="id">Datensatz ID</param>
        protected virtual void LoadDataRow(int id)
        {
            RaisePropertyChanged();
        }

        #endregion


        #region Properties

        /// <summary>
        /// Aktuelle Page
        /// </summary>
        public Pages.IBasePage2 CurrentPage { get; set; }

        /// <summary>
        /// Datensatz ID
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Kopiermodus
        /// </summary>
        public virtual bool IsCopy { get; set; }

        /// <summary>
        /// Titel der aktuellen Page
        /// </summary>
        public string Title
        {
            get
            {
                if (CurrentPage != null)
                    return CurrentPage.GetTitle();
                else
                    return "Datensatz";
            }
        }

        /// <summary>
        /// Beschreibung der aktuellen Page
        /// </summary>
        public virtual string Description
        {
            get
            {
                if (Id > 0)
                {
                    if (IsCopy)
                        return Title + " kopieren";
                    else
                        return Title + " bearbeiten";
                }
                else
                {
                    if (IsCopy)
                        return Title + " kopieren";
                    else
                        return Title + " hinzufügen";
                }
            }
        }

        /// <summary>
        /// Anzeige der Ja / Nein bzw. Speichern / Abbrechen Buttons
        /// </summary>
        public System.Windows.Visibility DialogYesNoButtons
        {
            get
            {
                return System.Windows.Visibility.Visible;
            }
        }

        /// <summary>
        /// Anzeige des Ok Buttons
        /// </summary>
        public System.Windows.Visibility DialogOkButtons
        {
            get
            {
                return System.Windows.Visibility.Collapsed;
            }
        }

        #endregion


        #region Commands

        public ICommand DialogSaveCommand { get; private set; }
        public ICommand DialogCancelCommand { get; private set; }
        public ICommand DialogOkCommand { get; private set; }

        #endregion


        #region Logik

        /// <summary>
        /// Ok Click
        /// </summary>
        protected virtual void DialogOkClick()
        {
            MessengerInstance.Send(new DialogWindowCloseMessage());
        }

        /// <summary>
        /// Kann Ok Click ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanDialogOkClick()
        {
            return true;
        }

        /// <summary>
        /// Cancel Click
        /// </summary>
        protected virtual void DialogCancelClick()
        {
            MessengerInstance.Send(new DialogWindowCloseMessage());
        }

        /// <summary>
        /// Save Click
        /// </summary>
        protected virtual void DialogSaveClick()
        {
            MessengerInstance.Send(new DialogWindowCloseMessage());
        }

        /// <summary>
        /// Kann Save Click ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanDialogSaveClick()
        {
            return true;
        }

        #endregion
    }
}
