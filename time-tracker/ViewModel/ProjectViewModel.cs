﻿using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker.ViewModel
{
    public class ProjectViewModel : DialogFrameViewModel
    {
        #region Member

        private ProjectController projectController = null;
        private Models.Project projectObj = null;

        #endregion


        #region Init & Close

        /// <summary>
        /// Konstruktor
        /// </summary>
        public ProjectViewModel()
        {
            projectController = new ProjectController();
            projectObj = new Models.Project();
        }

        public override void Cleanup()
        {
            projectObj.Init();
            RaisePropertyChanged();
            base.Cleanup();
        }

        /// <summary>
        /// Datensatz mit der angegebenen ID aus der Datenbank lesen
        /// </summary>
        /// <param name="id">Datensatz ID</param>
        protected override void LoadDataRow(int id)
        {
            projectObj = projectController.GetSingle(id);

            if (IsCopy)
            {
                projectObj.ID = 0;
                projectObj.Synced = 0;
                projectObj.ObjectId = -1;
            }

            base.LoadDataRow(id);
        }

        #endregion


        #region Properties

        /// <summary>
        /// Projekt ID
        /// </summary>
        public override int Id
        {
            get { return projectObj.ID; }
            set
            {
                if (value > 0)
                {
                    LoadDataRow(value);
                }
                else
                {
                    projectObj.ID = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Projekt Aktiv
        /// </summary>
        public int Active
        {
            get { return projectObj.Active; }
            set
            {
                projectObj.Active = value;
                RaisePropertyChanged(nameof(Active));
            }
        }

        /// <summary>
        /// Ist das Projekt Aktiv?
        /// </summary>
        public bool IsActive
        {
            get { return Active == 1; }
            set
            {
                if (value)
                    Active = 1;
                else
                    Active = 0;

                RaisePropertyChanged(nameof(Active));
            }
        }

        /// <summary>
        /// Projekt Name
        /// </summary>
        public string Name
        {
            get { return projectObj.Name; }
            set
            {
                projectObj.Name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        /// <summary>
        /// Projekt Privat
        /// </summary>
        public int Private
        {
            get { return projectObj.Private; }
            set
            {
                projectObj.Private = value;
                RaisePropertyChanged(nameof(Private));
            }
        }

        /// <summary>
        /// Ist das Projekt Privat?
        /// </summary>
        public bool IsPrivate
        {
            get { return Private == 1; }
            set
            {
                if (value)
                    Private = 1;
                else
                    Private = 0;

                RaisePropertyChanged(nameof(Private));
            }
        }

        #endregion


        #region Logik

        /// <summary>
        /// Daten speichern
        /// </summary>
        protected override void DialogSaveClick()
        {
            UiServices.SetBusyState();

            if (projectObj.ID == 0)
            {
                // insert
                int x = projectController.Insert(projectObj);
            }
            else
            {
                // update
                projectObj.Synced = 0;
                int x = projectController.Update(projectObj);
            }

            MessengerInstance.Send(new ProjectsEditedMessage());
            base.DialogSaveClick();
        }

        /// <summary>
        /// Kann Speichern ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected override bool CanDialogSaveClick()
        {
            bool returnValue = true;

            // Validation fixe Parameter
            if (string.IsNullOrEmpty(projectObj.Name))
                returnValue = false;

            return returnValue;
        }

        #endregion
    }
}
