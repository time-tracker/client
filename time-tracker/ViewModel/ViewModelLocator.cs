/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:time_tracker"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using CommonServiceLocator;

namespace time_tracker.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<EditViewModel>();
            SimpleIoc.Default.Register<TaskViewModel>();
            SimpleIoc.Default.Register<ProjectViewModel>();
            SimpleIoc.Default.Register<EventViewModel>();
            SimpleIoc.Default.Register<TaskDivideViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public EditViewModel Edit
        {
            get
            {
                return ServiceLocator.Current.GetInstance<EditViewModel>();
            }
        }

        public TaskViewModel TaskVM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TaskViewModel>();
            }
        }

        public ProjectViewModel ProjectVM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ProjectViewModel>();
            }
        }

        public EventViewModel EventVM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<EventViewModel>();
            }
        }

        public TaskDivideViewModel TaskDivideVM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TaskDivideViewModel>();
            }
        }

        public static void Cleanup()
        {
            ServiceLocator.Current.GetInstance<TaskDivideViewModel>().Cleanup();
            ServiceLocator.Current.GetInstance<EventViewModel>().Cleanup();
            ServiceLocator.Current.GetInstance<ProjectViewModel>().Cleanup();
            ServiceLocator.Current.GetInstance<TaskViewModel>().Cleanup();
            ServiceLocator.Current.GetInstance<EditViewModel>().Cleanup();
            ServiceLocator.Current.GetInstance<MainViewModel>().Cleanup();
        }
    }
}