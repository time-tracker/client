﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker.ViewModel
{
    public class TaskDivideViewModel : DialogFrameViewModel
    {
        #region Member

        private TaskController taskController = null;
        private Models.Task taskObj = null;
        private ProjectController projectController = null;
        private DateTime _divideAtDate = DateTime.MinValue;
        private DateTime _divideAtDateTime = DateTime.MinValue;
        private List<string> divideHourList = null;
        private List<string> divideMinuteList = null;
        private string _selectedDivideHour = string.Empty;
        private string _selectedDivideMinute = string.Empty;

        #endregion


        #region Init & Close

        /// <summary>
        /// Konstruktor
        /// </summary>
        public TaskDivideViewModel()
        {
            taskController = new TaskController();
            projectController = new ProjectController();
            taskObj = new Models.Task();
        }

        public override void Cleanup()
        {
            taskObj.Init();
            SetDateAndHourFields();
            RaisePropertyChanged();
            base.Cleanup();
        }

        /// <summary>
        /// Datensatz mit der angegebenen ID aus der Datenbank lesen
        /// </summary>
        /// <param name="id">Datensatz ID</param>
        protected override void LoadDataRow(int id)
        {
            taskObj = taskController.GetSingle(id);

            if (taskObj != null)
            {
                if (taskObj.From.Date.Equals(taskObj.To.Date))
                    divideHourList = Helper.TimeCalc.getHourList(taskObj.From.Hour, taskObj.To.Hour, true);
                else
                    divideHourList = Helper.TimeCalc.getHourList(true);
            }
            else
                divideHourList = Helper.TimeCalc.getHourList(true);

            divideMinuteList = Helper.TimeCalc.getMinuteList();

            SetDateAndHourFields();

            base.LoadDataRow(id);
        }

        /// <summary>
        /// Combo Boxen für Uhrzeit setzen
        /// </summary>
        private void SetDateAndHourFields()
        {
            DivideDate = taskObj.From.Date;
            SelectedDivideHour = taskObj.From.ToString("HH");
            SelectedDivideMinute = taskObj.From.ToString("mm");

            calcDivideAtDateTime();
        }

        #endregion


        #region Properties

        /// <summary>
        /// Task ID
        /// </summary>
        public override int Id
        {
            get { return taskObj.ID; }
            set
            {
                taskObj.ID = value;
                LoadDataRow(value);
            }
        }

        /// <summary>
        /// Beschreibung der aktuellen Page
        /// </summary>
        public override string Description
        {
            get
            {
                return Title + " teilen";
            }
        }

        /// <summary>
        /// Kann Datum Geteilt Um bearbeitet werden?
        /// </summary>
        public bool? IsDivideDateEnabled
        {
            get
            {
                if (DivideDate.Date.Equals(ToDate.Date))
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Datum Von
        /// </summary>
        public DateTime DivideDate
        {
            get { return _divideAtDate; }
            set
            {
                _divideAtDate = value;
                RaisePropertyChanged(nameof(DivideDate));
                calcDivideAtDateTime();
            }
        }

        /// <summary>
        /// Liste mit den Stunden Von
        /// </summary>
        public List<string> DivideHourList
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedDivideHour))
                    SelectedDivideHour = divideHourList.First();

                return divideHourList;
            }
        }

        /// <summary>
        /// selektierte Stunde Von
        /// </summary>
        public string SelectedDivideHour
        {
            get { return _selectedDivideHour; }
            set
            {
                _selectedDivideHour = value;
                RaisePropertyChanged(nameof(SelectedDivideHour));
                calcDivideAtDateTime();
            }
        }

        /// <summary>
        /// Liste mit Minuten Von
        /// </summary>
        public List<string> DivideMinuteList
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedDivideMinute))
                    SelectedDivideMinute = divideMinuteList.First();

                return divideMinuteList;
            }
        }

        /// <summary>
        /// selektierte Minute Von
        /// </summary>
        public string SelectedDivideMinute
        {
            get { return _selectedDivideMinute; }
            set
            {
                _selectedDivideMinute = value;
                RaisePropertyChanged(nameof(SelectedDivideMinute));
                calcDivideAtDateTime();
            }
        }        

        /// <summary>
        /// Von Datum und Zeit zusammensetzen
        /// </summary>
        private void calcDivideAtDateTime()
        {
            if ((string.IsNullOrEmpty(SelectedDivideHour) == false) &&
                (string.IsNullOrEmpty(SelectedDivideMinute) == false))
            {
                _divideAtDateTime = _divideAtDate.Date.AddHours(Convert.ToDouble(SelectedDivideHour)).AddMinutes(Convert.ToDouble(SelectedDivideMinute));
            }
        }

        /// <summary>
        /// Datum Von
        /// </summary>
        public DateTime FromDate
        {
            get { return taskObj.From; }
            set
            {
                taskObj.From = value;
                RaisePropertyChanged(nameof(FromDate));
            }
        }

        /// <summary>
        /// Datum Bis
        /// </summary>
        public DateTime ToDate
        {
            get { return taskObj.To; }
            set
            {
                taskObj.To = value;
                RaisePropertyChanged(nameof(ToDate));
            }
        }

        /// <summary>
        /// Projekt
        /// </summary>
        public Models.Project Project
        {
            get
            {
                return taskObj.Project;
            }
            set
            {
                taskObj.Project = value;
                RaisePropertyChanged(nameof(Project));
            }
        }

        /// <summary>
        /// Notiz, Bemerkung
        /// </summary>
        public string Note
        {
            get { return taskObj.Note; }
            set
            {
                taskObj.Note = value;
                RaisePropertyChanged(nameof(Note));
            }
        }

        /// <summary>
        /// Zeit Gebucht / Abgerechnet
        /// </summary>
        public int Booked
        {
            get { return taskObj.Booked; }
            set
            {
                taskObj.Booked = value;
                RaisePropertyChanged(nameof(Booked));
            }
        }

        /// <summary>
        /// Ist die Zeit gebucht / abgerechnet?
        /// </summary>
        public bool IsBooked
        {
            get { return Booked == 1; }
            set
            {
                if (value)
                    Booked = 1;
                else
                    Booked = 0;

                RaisePropertyChanged(nameof(Booked));
            }
        }

        /// <summary>
        /// Ort
        /// </summary>
        public string Location
        {
            get { return taskObj.Location; }
            set
            {
                taskObj.Location = value;
                RaisePropertyChanged(nameof(Location));
            }
        }

        #endregion


        #region Logik

        /// <summary>
        /// Daten speichern
        /// </summary>
        protected override void DialogSaveClick()
        {
            UiServices.SetBusyState();

            try
            {
                // insert der neuen Zeit
                Models.Task newTask = new Models.Task()
                {
                    From = _divideAtDateTime,
                    To = taskObj.To,
                    Project = taskObj.Project,
                    Note = taskObj.Note,
                    Location = taskObj.Location,
                    Booked = taskObj.Booked,
                };
                int i = taskController.Insert(newTask);

                // update der geteilten Zeit
                taskObj.To = _divideAtDateTime;
                int u = taskController.Update(taskObj);
            }
            catch (Exception err)
            {
                Console.WriteLine("Divide Error:" + err.Message);
                MessengerInstance.Send(new TaskDivideErrorMessage());
            }

            MessengerInstance.Send(new TasksEditedMessage());
            base.DialogSaveClick();
        }

        /// <summary>
        /// Kann Speichern ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected override bool CanDialogSaveClick()
        {
            bool returnValue = true;

            // Validation fixe Parameter
            if ((_divideAtDateTime <= FromDate) || (_divideAtDateTime >= ToDate))
                return false;

            return returnValue;
        }

        #endregion
    }
}
