using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using time_tracker.Controller;

namespace time_tracker.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Members

        private TaskController taskController = null;
        private ProjectController projectController = null;
        private List<Models.Project> _Projects = null;
        private Models.Project _SelectedProject = null;
        private const string STAMP_ON_TEXT = "anstempeln";
        private const string STAMP_OFF_TEXT = "abstempeln";
        private string stampText;
        private static System.Windows.Media.Brush STAMP_ON_COLOR = System.Windows.Media.Brushes.Green;
        private static System.Windows.Media.Brush STAMP_OFF_COLOR = System.Windows.Media.Brushes.Red;
        private System.Windows.Media.Brush stampBackgroundColor;
        private const string DEFAULT_NOTE = "Notiz ...";
        private string note = null;

        #endregion

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            ///
            taskController = new TaskController();
            projectController = new ProjectController();
            _Projects = new List<Models.Project>();

            // Vorbelegung
            stampText = STAMP_ON_TEXT;
            stampBackgroundColor = STAMP_ON_COLOR;

            Global.Param.taskId = -1;
            note = DEFAULT_NOTE;
        }


        #region Properties

        /// <summary>
        /// Liste mit Projekten
        /// </summary>
        public List<Models.Project> Projects
        {
            get
            {
                // alle Projekte holen
                _Projects = projectController.GetAll();

                // erstes Projekt selektieren
                if (_Projects.Count > 0)
                    SelectedProject = _Projects.First();

                return _Projects;
            }
        }

        /// <summary>
        /// Selektiertes Projekt
        /// </summary>
        public Models.Project SelectedProject
        {
            get { return _SelectedProject; }
            set
            {
                _SelectedProject = value;
                RaisePropertyChanged(nameof(SelectedProject));
            }
        }

        /// <summary>
        /// Notiz
        /// </summary>
        public string Note
        {
            get { return note; }
            set
            {
                note = value;
                RaisePropertyChanged(nameof(Note));
            }
        }

        /// <summary>
        /// Notiz Sichtbarkeit
        /// </summary>
        public Visibility Note_Visibility
        {
            get
            {
                if (IsStamped)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// An/Abstempeln Button Text
        /// </summary>
        public string Stamp_Text
        {
            get { return stampText; }
        }

        /// <summary>
        /// An/Abstempeln Button Hintergrundfarbe
        /// </summary>
        public System.Windows.Media.Brush Stamp_BGColor
        {
            get { return stampBackgroundColor; }
        }

        /// <summary>
        /// Flag, ob gerade Angestempel bzw. Abgestempelt ist
        /// </summary>
        public bool IsStamped
        {
            get
            {
                if (stampText.Equals(STAMP_ON_TEXT))
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Flag, IsStamped nur umgekehrt
        /// </summary>
        public bool IsNotStamped
        {
            get { return IsStamped == false; }
        }

        /// <summary>
        /// Text, wann die App gestartet wurde
        /// </summary>
        public string App_On_Text
        {
            get
            {
                Models.Task task = taskController.GetSingle(Global.Param.pcOnOffTaskId);
                return "PC an seit " + task.From.ToShortTimeString() + " Uhr";
            }
        }

        #endregion

        #region Commands

        private ICommand _leftClick = null;
        private ICommand _closeClick = null;
        private ICommand _stampClick = null;
        private ICommand _openEditWindowCommand = null;

        /// <summary>
        /// Command zum Master Rezept l�schen
        /// </summary>
        public ICommand LeftClickCommand
        {
            get
            {
                if (_leftClick == null)
                {
                    _leftClick = new RelayCommand(() => LeftClick());
                }

                return _leftClick;
            }
        }

        /// <summary>
        /// Command zum Programm beenden
        /// </summary>
        public ICommand CloseClickCommand
        {
            get
            {
                if (_closeClick == null)
                {
                    _closeClick = new RelayCommand(() => CloseClick());
                }

                return _closeClick;
            }
        }

        /// <summary>
        /// Command zum An/Abstempeln
        /// </summary>
        public ICommand StampClickCommand
        {
            get
            {
                if (_stampClick == null)
                {
                    _stampClick = new RelayCommand(() => StampClick(), () => CanStampClick());
                }

                return _stampClick;
            }
        }

        /// <summary>
        /// Command zum �ffnen des Edit Windows
        /// </summary>
        public ICommand OpenEditWindowCommand
        {
            get
            {
                if (_openEditWindowCommand == null)
                {
                    _openEditWindowCommand = new Base.RelayCommand(p => OpenEditWindow(p), p => CanOpenEditWindow());
                }

                return _openEditWindowCommand;
            }
        }

        #endregion

        #region Logik

        /// <summary>
        /// linker Mausklick
        /// </summary>
        private void LeftClick()
        {
            // Main Window anzeigen
            MessengerInstance.Send(new WindowVisibleMessage(true));

            // nur wenn keine Zeit angestempelt ist sollen die Projekte aktualisiert werden
            if (Global.Param.taskId == -1)
                RaisePropertyChanged(nameof(Projects));
        }

        /// <summary>
        /// Programm beenden
        /// </summary>
        private void CloseClick()
        {
            MessengerInstance.Send(new AppCloseMessage());
        }

        /// <summary>
        /// An/Abstempeln
        /// </summary>
        private void StampClick()
        {
            if (IsStamped == false)
            {
                // Prozess: Anstempeln

                // Zeit speichern
                if (Global.Param.taskId == -1)
                {
                    Models.Task task = new Models.Task();
                    task.From = DateTime.Now;
                    task.Project = _SelectedProject;

                    Global.Param.taskId = taskController.Insert(task);
                    if (Global.Param.taskId <= 0)
                    {
                        throw new Exception("Fehler beim Hinzuf�gen der Zeit! Fehler Code: " + Global.Param.taskId);
                    }
                }
                else
                    throw new Exception("Es ist noch eine ungespeicherte Zeit vorhanden! Task ID: " + Global.Param.taskId);


                // Stamp Button anpassen
                stampText = STAMP_OFF_TEXT;
                RaisePropertyChanged(nameof(Stamp_Text));
                stampBackgroundColor = STAMP_OFF_COLOR;
                RaisePropertyChanged(nameof(Stamp_BGColor));

                // Main Window ausblenden
                MessengerInstance.Send(new WindowVisibleMessage(false));
            }
            else
            {
                // Prozess: Abstempeln

                // Zeit speichern
                if (Global.Param.taskId > 0)
                {
                    Models.Task task = taskController.GetSingle(Global.Param.taskId);
                    task.To = DateTime.Now;
                    if (note.Equals(DEFAULT_NOTE) == false)
                        task.Note = note;

                    Global.Param.taskId = taskController.Update(task);
                    if (Global.Param.taskId <= 0)
                    {
                        throw new Exception("Fehler beim Aktualisieren der Zeit! Fehler Code: " + Global.Param.taskId);
                    }
                    else
                    {
                        // Task ID zur�cksetzen
                        Global.Param.taskId = -1;

                        // Notiz zur�cksetzen
                        note = DEFAULT_NOTE;
                    }
                }
                else
                    throw new Exception("Es ist keine Zeit zum Abstempeln vorhanden! Task ID: " + Global.Param.taskId);

                // Stamp Button anpassen
                stampText = STAMP_ON_TEXT;
                RaisePropertyChanged(nameof(Stamp_Text));
                stampBackgroundColor = STAMP_ON_COLOR;
                RaisePropertyChanged(nameof(Stamp_BGColor));

                // Zeiten aktualisieren
                MessengerInstance.Send(new TasksEditedMessage());
            }

            RaisePropertyChanged(nameof(IsStamped));
            RaisePropertyChanged(nameof(IsNotStamped));
            RaisePropertyChanged(nameof(Note));
            RaisePropertyChanged(nameof(Note_Visibility));
        }

        /// <summary>
        /// Kann an/abgestempelt werden?
        /// </summary>
        /// <returns></returns>
        private bool CanStampClick()
        {
            if (_Projects.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Edit Window �ffnen
        /// </summary>
        private void OpenEditWindow(object parameter)
        {
            int source = 0;

            try
            {
                source = Convert.ToInt32(parameter.ToString());
            }
            catch (Exception)
            {
                source = 0;
            }

            // Main Window ausblenden
            MessengerInstance.Send(new OpenEditWindowMessage(source));
        }

        /// <summary>
        /// Kann das Edit Window ge�ffnet werden?
        /// </summary>
        /// <returns></returns>
        private bool CanOpenEditWindow()
        {
            return true;
        }

        #endregion
    }
}