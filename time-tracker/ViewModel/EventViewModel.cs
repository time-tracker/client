﻿using System;
using System.Collections.Generic;
using System.Linq;
using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker.ViewModel
{
    public class EventViewModel : DialogFrameViewModel
    {
        #region Member

        private EventController eventController = null;
        private Models.Event eventObj = null;

        #endregion


        #region Init & Close

        /// <summary>
        /// Konstruktor
        /// </summary>
        public EventViewModel()
        {
            eventController = new EventController();
            eventObj = new Models.Event();
        }

        public override void Cleanup()
        {
            eventObj.Init();
            RaisePropertyChanged();
            base.Cleanup();
        }

        /// <summary>
        /// Datensatz mit der angegebenen ID aus der Datenbank lesen
        /// </summary>
        /// <param name="id">Datensatz ID</param>
        protected override void LoadDataRow(int id)
        {
            eventObj = eventController.GetSingle(id);
            
            if (IsCopy)
            {
                eventObj.ID = 0;
                eventObj.Synced = 0;
                eventObj.ObjectId = -1;
            }

            base.LoadDataRow(id);
        }

        #endregion


        #region Properties

        /// <summary>
        /// Event ID
        /// </summary>
        public override int Id
        {
            get { return eventObj.ID; }
            set
            {
                if (value > 0)
                {
                    LoadDataRow(value);
                }
                else
                {
                    eventObj.ID = value;
                    eventObj.Category = null;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Datum
        /// </summary>
        public DateTime On
        {
            get { return eventObj.On; }
            set
            {
                eventObj.On = value;
                RaisePropertyChanged(nameof(On));
            }
        }

        /// <summary>
        /// Auswahl an Kategorien
        /// </summary>
        public List<Models.EventCategory> Categories
        {
            get
            {
                List<Models.EventCategory> categories = Global.Param.getEventCategories();

                if (eventObj.Category == null)
                    eventObj.Category = categories.First();
                else
                {
                    // Workaround: wenn bereits eine Kategorie zugewiesen ist, dann aus der Kategorienliste nochmals zuweisen.
                    //             Ansonsten funktioniert die Kategorie Selektion nicht.
                    Category = categories.Find(c => c.ID == eventObj.Category.ID);
                }

                return categories;
            }
        }

        /// <summary>
        /// Kategorie
        /// </summary>
        public Models.EventCategory Category
        {
            get { return eventObj.Category; }
            set
            {
                eventObj.Category = value;
                RaisePropertyChanged(nameof(Category));
            }
        }

        /// <summary>
        /// Notiz / Bemerkung
        /// </summary>
        public string Note
        {
            get { return eventObj.Note; }
            set
            {
                eventObj.Note = value;
                RaisePropertyChanged(nameof(Note));
            }
        }

        #endregion


        #region Logik

        /// <summary>
        /// Daten speichern
        /// </summary>
        protected override void DialogSaveClick()
        {
            UiServices.SetBusyState();

            try
            {
                if (eventObj.ID == 0)
                {
                    // insert
                    try
                    {
                        int x = eventController.Insert(eventObj);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else
                {
                    // update
                    try
                    {
                        eventObj.Synced = 0;
                        int x = eventController.Update(eventObj);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception error)
            {
                // bugfix #26 UNIQUE FEHLER
                if (error.Message.Contains("UNIQUE"))
                {
                    MessengerInstance.Send(new EventUniqueErrorMessage());
                }
            }

            MessengerInstance.Send(new EventsEditedMessage());
            base.DialogSaveClick();
        }

        /// <summary>
        /// Kann Speichern ausgeführt werden?
        /// </summary>
        /// <returns></returns>
        protected override bool CanDialogSaveClick()
        {
            bool returnValue = true;

            // Validation fixe Parameter
            if (eventObj.On == null)
                returnValue = false;
            if (eventObj.Category == null)
                returnValue = false;

            return returnValue;
        }

        #endregion
    }
}
