﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Helper
{
    public class Sqlite
    {
        #region Zeitstempel Konvertierung

        /// <summary>
        /// UNIX Epoch Zeitstempel
        /// </summary>
        private static DateTime unixEpoc = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        /// <summary>
        /// Konvertierung von UNIX Zeitstempel zu DateTime
        /// </summary>
        /// <param name="Timestamp"></param>
        /// <returns></returns>
        public static DateTime Timestamp2DateTime(int Timestamp)
        {
            // den Timestamp addieren           
            DateTime returnDateTime = unixEpoc.AddSeconds(Timestamp);

            return returnDateTime;
        }

        /// <summary>
        /// Konvertierung von DateTime zu UNIX Zeistempel
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int DateTime2Timestamp(DateTime dateTime)
        {
            // Differenz ermitteln
            TimeSpan diffSpan = new TimeSpan(dateTime.Ticks - unixEpoc.Ticks);

            // Die Differenz in Gesamtanzahl der Sekunden ist der Unix-Timestamp
            return Convert.ToInt32(diffSpan.TotalSeconds);
        }

        #endregion
    }
}
