﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Helper
{
    /// <summary>
    /// Hilfe Klasse für Zeit Berechnungen
    /// </summary>
    public class TimeCalc
    {
        /// <summary>
        /// Ticks zu einem schönen String "X Std. X Min." umwandeln
        /// </summary>
        /// <param name="ticks"></param>
        /// <returns></returns>
        public static string TicksToStr(long ticks)
        {
            TimeSpan span = new TimeSpan(ticks);
            double totalHours = Math.Truncate(span.TotalMinutes / 60);
            double minutes = Math.Truncate(span.TotalMinutes - (totalHours * 60));

            if (minutes != 0)
                return totalHours + " Std. " + minutes + " Min.";
            else
                return totalHours + " Std. ";
        }

        /// <summary>
        /// Kalenderwoche ausgeben
        /// </summary>
        /// <param name="date">Datum</param>
        /// <returns>Kalenderwoche</returns>
        public static int getWeek(DateTime date)
        {
            CultureInfo cui = CultureInfo.CurrentCulture;
            return cui.Calendar.GetWeekOfYear(date, cui.DateTimeFormat.CalendarWeekRule, cui.DateTimeFormat.FirstDayOfWeek);
        }

        /// <summary>
        /// Monatsbezeichnung anhand des Monats ausgeben
        /// </summary>
        /// <param name="month">Monat</param>
        /// <returns>Monatsbezeichnung</returns>
        public static string getMonthStr(int month)
        {
            switch (month)
            {
                case 1: return "Januar";
                case 2: return "Februar";
                case 3: return "März";
                case 4: return "April";
                case 5: return "Mai";
                case 6: return "Juni";
                case 7: return "Juli";
                case 8: return "August";
                case 9: return "September";
                case 10: return "Oktober";
                case 11: return "November";
                case 12: return "Dezember";
                default: return Convert.ToString(month);
            }
        }

        /// <summary>
        /// Gibt eine Liste mit Stunden zurück
        /// </summary>
        /// <param name="withFilling0">mit führenden 0en?</param>
        /// <returns>Liste mit Stunden</returns>
        public static List<string> getHourList(bool withFilling0 = false)
        {
            return getHourList(0, 23, withFilling0);
        }

        /// <summary>
        /// Gibt eine Liste mit Stunden zurück - mit Start und End Stunde
        /// </summary>
        /// <param name="startHour">Start Stunde</param>
        /// <param name="endHour">End Stunde</param>
        /// <param name="withFilling0">mit führenden 0en?</param>
        /// <returns>Liste mit Stunden</returns>
        public static List<string> getHourList(int startHour, int endHour, bool withFilling0 = false)
        {
            List<string> l = new List<string>();

            for (int i = startHour; i <= endHour; i++)
            {
                if (withFilling0)
                {
                    if (i < 10)
                        l.Add("0" + i);
                    else
                        l.Add(i.ToString());
                }
                else
                    l.Add(i.ToString());
            }

            return l;
        }

        /// <summary>
        /// Gibt eine Liste mit Minuten zurück
        /// </summary>
        /// <returns>Liste mit Minuten</returns>
        public static List<string> getMinuteList()
        {
            List<string> l = new List<string>();

            for (int i = 0; i < 60; i++)
            {
                if (i < 10)
                    l.Add("0" + i);
                else
                    l.Add(i.ToString());
            }

            return l;
        }
    }
}
