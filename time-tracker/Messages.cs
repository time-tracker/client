﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker
{
    #region Main Window

    /// <summary>
    /// Main Window anzeigen / ausblenden Message
    /// </summary>
    public class WindowVisibleMessage
    {
        public WindowVisibleMessage(bool isVisible)
        {
            IsVisible = isVisible;
        }

        public bool IsVisible { get; private set; }
    }

    /// <summary>
    /// App beenden Message
    /// </summary>
    public class AppCloseMessage
    {
    }

    #endregion


    #region Edit Window

    /// <summary>
    /// Edit Window öffnen
    /// </summary>
    public class OpenEditWindowMessage
    {
        public OpenEditWindowMessage(int source)
        {
            Source = source;
        }

        /// <summary>
        /// Auf welchen Menupunkt wurde geklickt?
        /// </summary>
        public int Source { get; private set; }
    }

    /// <summary>
    /// Edit Window beenden Message
    /// </summary>
    public class EditWindowCloseMessage
    {
    }

    /// <summary>
    /// Backend Fehler Message
    /// </summary>
    public class BackendError
    {
        public BackendError(string message)
        {
            Message = message;
        }

        public string Message { get; private set; }
    }

    /// <summary>
    /// Backend Login Fehler Message
    /// </summary>
    public class BackendLoginError
    {
    }

    #endregion


    #region Dialog Frame Window

    /// <summary>
    /// Optionen für Dialog Window öffnen Message
    /// </summary>
    public class DialogWindowOpenOptions
    {
        public DialogWindowOpenOptions()
        {
            Id = 0;
            IsCopy = false;
        }

        public Pages.PageRepository.Page Page { get; set; }
        public int Id { get; set; }
        public bool IsCopy { get; set; }
    }

    /// <summary>
    /// Dialog Window öffnen Message
    /// </summary>
    public class DialogWindowOpenMessage
    {
        public DialogWindowOpenMessage(DialogWindowOpenOptions options)
        {
            Options = options;
        }

        public DialogWindowOpenOptions Options { get; private set; }
    }

    /// <summary>
    /// Dialog Window beenden Message
    /// </summary>
    public class DialogWindowCloseMessage
    {
    }

    /// <summary>
    /// Zeiten wurden editiert Message
    /// </summary>
    public class TasksEditedMessage
    {
    }

    /// <summary>
    /// Projekte wurden editiert Message
    /// </summary>
    public class ProjectsEditedMessage
    {
    }

    /// <summary>
    /// Ereignisse wurden editiert Message
    /// </summary>
    public class EventsEditedMessage
    {
    }

    /// <summary>
    /// Doppeltes Ereignis Fehler Message (bugfix #26)
    /// </summary>
    public class EventUniqueErrorMessage
    {
    }

    /// <summary>
    /// Fehler beim Zeit teilen Message
    /// </summary>
    public class TaskDivideErrorMessage
    {
    }

    #endregion
}
