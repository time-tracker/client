﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Windows;
using time_tracker.Controller;
using time_tracker.Helper;

namespace time_tracker
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// App wirklich beenden?
        /// </summary>
        private bool reallyClosingApp = false;

        private SettingController settingsContr = null;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            Messenger.Default.Register<WindowVisibleMessage>(this, DoWindowVisible);
            Messenger.Default.Register<AppCloseMessage>(this, m =>
            {
                reallyClosingApp = true;
                this.Close();
            });
            Messenger.Default.Register<OpenEditWindowMessage>(this, DoOpenEditWindow);

            //
            //  Settings Controller
            //
            settingsContr = new SettingController();

            //
            // Ereignisse
            //
            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Layout je nach Höhe optimieren
            this.SizeToContent = SizeToContent.Height;

            // Settings-Instanz erzeugen
            var settings = Properties.Settings.Default;

            try
            {
                // Position und Größe des Main Fensters auslesen
                this.Left = settings.StartMainLeft;
                this.Top = settings.StartMainTop;
                this.Width = settings.StartMainWidth;
                this.Height = settings.StartMainHeight;
            }
            catch (Exception)
            {
            }

            // Fenster beim Start verbergen
            if (settingsContr.getBoolParameter(SettingController.OPEN_AT_START) == false)
            {
                this.Hide();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Settings-Instanz holen
            Properties.Settings settings = Properties.Settings.Default;

            try
            {
                // Position und Größe des Main Fensters in der Konfiguration ablegen
                settings.StartMainLeft = Convert.ToInt32(this.Left);
                settings.StartMainTop = Convert.ToInt32(this.Top);
                settings.StartMainWidth = Convert.ToInt32(this.Width);
                settings.StartMainHeight = Convert.ToInt32(this.Height);

                // Settings speichern
                settings.Save();
            }
            catch (Exception)
            {
            }

            // Programm schließen
            if (reallyClosingApp == false)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// Behandlung der Window Visible Message
        /// </summary>
        /// <param name="obj"></param>
        private void DoWindowVisible(WindowVisibleMessage obj)
        {
            if (obj.IsVisible)
            {
                this.Show();
                this.Activate();

                // Wenn das Notiz Feld sichtbar ist, dann fokussieren
                if (fNote.Focusable)
                {
                    fNote.SelectAll();
                    fNote.Focus();
                }
            }
            else
                this.Hide();
        }

        /// <summary>
        /// Behandlung der Open Edit Window Message
        /// </summary>
        private void DoOpenEditWindow(OpenEditWindowMessage obj)
        {
            UiServices.SetBusyState();

            // nur wenn Main nicht sichtbar ist, darf bearbeitet werden
            if (this.IsVisible == false)
            {
                // gültige Instanz besorgen
                EditWindow editWindow = EditWindow.Instance;

                // Menü Tab setzen
                editWindow.MenuIndex = obj.Source;

                // Wenn Fenster noch gar nie angezeigt wurde
                if (editWindow.IsVisible == false)
                {
                    editWindow.Owner = this;
                    editWindow.ShowDialog();
                }
                // Wenn Fenster minimiert ist
                else if (editWindow.WindowState == WindowState.Minimized)
                    editWindow.WindowState = WindowState.Normal;
                // Wenn Fenster von einer anderen Anwendung verdeckt wird
                else
                    editWindow.Activate();
            }
            else
            {
                this.Topmost = true;
                this.Topmost = false;
            }
        }
    }
}
