﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace time_tracker
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        private Controller.TaskController taskController = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Konstruktor
            taskController = new Controller.TaskController();

            // Datenbank checken
            Controller.DatabaseController.checkDatabase();

            // offene Zeiten bereinigen
            taskController.CleanupTasks();

            // 'PC an' Zeit stempeln
            Global.Param.pcOnOffTaskId = taskController.Insert(new Models.Task() { From = DateTime.Now, Note = "PC an/aus" });
        }

        protected override void OnExit(ExitEventArgs e)
        {
            // kommt wenn die Anwendung über Beenden geschlossen wird
            doClosingOperations();

            base.OnExit(e);
        }

        protected override void OnSessionEnding(SessionEndingCancelEventArgs e)
        {
            // kommt wenn die Anwendung per Logoff oder Shutdown geschlossen wird
            doClosingOperations();

            base.OnSessionEnding(e);
        }

        /// <summary>
        /// Beendigungs Operationen ausführen
        /// </summary>
        private void doClosingOperations()
        {
            stampTask();
            stampPcOffTask();
        }

        /// <summary>
        /// 'PC aus' Zeit stempeln
        /// </summary>
        private void stampPcOffTask()
        {
            Models.Task task = taskController.GetSingle(Global.Param.pcOnOffTaskId);
            task.To = DateTime.Now;
            taskController.Update(task);
        }

        /// <summary>
        /// Möglicherweise offene 'Projekt' Zeit abstempeln
        /// </summary>
        private void stampTask()
        {
            if (Global.Param.taskId > 0)
            {
                Models.Task task = taskController.GetSingle(Global.Param.taskId);
                task.To = DateTime.Now;
                taskController.Update(task);
            }
        }
    }
}
