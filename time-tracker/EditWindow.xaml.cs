﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using time_tracker.Pages;
using time_tracker.ViewModel;

namespace time_tracker
{
    /// <summary>
    /// Interaktionslogik für EditWindow.xaml, Singleton Klasse
    /// </summary>
    public partial class EditWindow : Window
    {
        #region Member

        private static EditWindow instance;
        private static readonly object padlock = new object();

        #endregion

        /// <summary>
        /// Konstruktor
        /// </summary>
        private EditWindow()
        {
            InitializeComponent();

            //
            // Messages
            //
            Messenger.Default.Register<EditWindowCloseMessage>(this, m =>
            {
                this.Close();
            });
            Messenger.Default.Register<DialogWindowOpenMessage>(this, DialogWindowOpen);

            // Backend Fehler Message
            Messenger.Default.Register<BackendError>(this, m =>
            {
                MessageBox.Show("Tschuldige, die Kommunikation mit Time-Tracker Web hat nicht funktioniert. Melde dich bei Sebastian Hofer." + Environment.NewLine +
                    "Fehler: " + m.Message, "Upps: Time-Tracker Web", MessageBoxButton.OK, MessageBoxImage.Error);
            });

            // Backend Login Fehler Message
            Messenger.Default.Register<BackendLoginError>(this, m =>
            {
                MessageBox.Show("Tschuldige, das Login in Time-Tracker Web hat nicht funktioniert. War deine " +
                    "Email Adresse und dein Passwort korrekt?", "Upps: Login Time-Tracker Web",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            });

            //
            // Ereignisse
            //
            this.Loaded += EditWindow_Loaded;
            this.Closing += EditWindow_Closing;
        }

        /// <summary>
        /// Singleton
        /// </summary>
        public static EditWindow Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                        instance = new EditWindow();

                    return instance;
                }
            }
        }

        /// <summary>
        /// Window Loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Settings-Instanz holen
            var settings = Properties.Settings.Default;

            try
            {
                // Position und Größe des Edit Fensters auslesen
                this.Left = settings.StartEditLeft;
                this.Top = settings.StartEditTop;
                this.Width = settings.StartEditWidth;
                this.Height = settings.StartEditHeight;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Window Closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Settings-Instanz erzeugen
            Properties.Settings settings = Properties.Settings.Default;

            try
            {
                // Position und Größe des Edit Fensters in der Konfiguration ablegen
                settings.StartEditLeft = this.Left;
                settings.StartEditTop = this.Top;
                settings.StartEditWidth = this.Width;
                settings.StartEditHeight = this.Height;

                // Settings speichern
                settings.Save();
            }
            catch (Exception)
            {
            }

            e.Cancel = true;
            this.Hide();
        }

        /// <summary>
        /// Index des angezeigten Menü Tabs
        /// </summary>
        public int MenuIndex
        {
            get { return pTabs.SelectedIndex; }
            set { pTabs.SelectedIndex = value; }
        }

        /// <summary>
        /// Dialog Window öffnen
        /// </summary>
        /// <param name="obj"></param>
        private void DialogWindowOpen(DialogWindowOpenMessage obj)
        {
            // Window erstellen
            DialogFrame2 fFrame = new DialogFrame2();
            fFrame.Owner = this;

            // Page aufrufen und Optionen setzen
            switch (obj.Options.Page)
            {
                case PageRepository.Page.Task:
                    ServiceLocator.Current.GetInstance<TaskViewModel>().IsCopy = obj.Options.IsCopy;
                    ServiceLocator.Current.GetInstance<TaskViewModel>().Id = obj.Options.Id;
                    ServiceLocator.Current.GetInstance<TaskViewModel>().CurrentPage = new Task2();
                    fFrame.DataContext = ServiceLocator.Current.GetInstance<TaskViewModel>();
                    break;
                case PageRepository.Page.Project:
                    ServiceLocator.Current.GetInstance<ProjectViewModel>().IsCopy = obj.Options.IsCopy;
                    ServiceLocator.Current.GetInstance<ProjectViewModel>().Id = obj.Options.Id;
                    ServiceLocator.Current.GetInstance<ProjectViewModel>().CurrentPage = new Project2();
                    fFrame.DataContext = ServiceLocator.Current.GetInstance<ProjectViewModel>();
                    break;
                case PageRepository.Page.Event:
                    ServiceLocator.Current.GetInstance<EventViewModel>().IsCopy = obj.Options.IsCopy;
                    ServiceLocator.Current.GetInstance<EventViewModel>().Id = obj.Options.Id;
                    ServiceLocator.Current.GetInstance<EventViewModel>().CurrentPage = new Event2();
                    fFrame.DataContext = ServiceLocator.Current.GetInstance<EventViewModel>();
                    break;
                case PageRepository.Page.TaskDivide:
                    ServiceLocator.Current.GetInstance<TaskDivideViewModel>().IsCopy = obj.Options.IsCopy;
                    ServiceLocator.Current.GetInstance<TaskDivideViewModel>().Id = obj.Options.Id;
                    ServiceLocator.Current.GetInstance<TaskDivideViewModel>().CurrentPage = new TaskDivide();
                    fFrame.DataContext = ServiceLocator.Current.GetInstance<TaskDivideViewModel>();
                    break;
                default:
                    break;
            }

            // Window aufrufen
            fFrame.ShowDialog();

            // Aufräumen
            switch (obj.Options.Page)
            {
                case PageRepository.Page.Task:
                    ServiceLocator.Current.GetInstance<TaskViewModel>().Cleanup();
                    break;
                case PageRepository.Page.Project:
                    ServiceLocator.Current.GetInstance<ProjectViewModel>().Cleanup();
                    break;
                case PageRepository.Page.Event:
                    ServiceLocator.Current.GetInstance<EventViewModel>().Cleanup();
                    break;
                case PageRepository.Page.TaskDivide:
                    ServiceLocator.Current.GetInstance<TaskDivideViewModel>().Cleanup();
                    break;
                default:
                    break;
            }
        }
    }
}
