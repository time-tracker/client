﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Pages
{
    public class PageRepository
    {
        /// <summary>
        /// Enum mit allen Pages
        /// </summary>
        public enum Page
        {
            // Zeit
            Task = 0,
            // Projekt
            Project = 1,
            // Event
            Event = 2,
            // Zeit teilen
            TaskDivide = 3,
        }
    }
}
