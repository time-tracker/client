﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace time_tracker.Pages
{
    public interface IBasePage2
    {
        /// <summary>
        /// Gibt die eindeutige Page zurück
        /// </summary>
        /// <returns>Page</returns>
        PageRepository.Page GetPage();

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Window Title</returns>
        string GetTitle();
    }
}
