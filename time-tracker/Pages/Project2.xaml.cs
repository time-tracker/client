﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace time_tracker.Pages
{
    /// <summary>
    /// Interaktionslogik für Project2.xaml
    /// </summary>
    public partial class Project2 : Page, IBasePage2
    {
        public Project2()
        {
            InitializeComponent();

            //
            // Fokus
            //
            fName.Focus();
        }

        public PageRepository.Page GetPage()
        {
            return PageRepository.Page.Project;
        }

        public string GetTitle()
        {
            return "Projekt";
        }
    }
}
