# Changelog

### Version 4.14.0

* Fehler das nach dem Update keine Daten mehr angezeigt werden behoben

### Version 4.13.0

* Fehler beim mehrfachen Öffnen des Editierfensters über das Kontektmenü behoben
* Fehler in der Berechnung der Statistiken behoben - alles orientiert sich an den Tagen

### Version 4.8.0

* Einfärben der PC An/Aus Zeiten (#34)
* Anbindung an Time-Tracker Web

### Version 4.7.0

* Nur aktive Projekt werden im Projektfilter dargestellt
* Optimierung beim Öffnen des Bearbeitungsfensters obwohl das Hauptfenster im Hintergrund war

### Version 4.6.0

* Fehler beim Speichern der Fensterpositionen behoben (#32)
* Ort Spalte hat jetzt eine fixe Breite. Variabel ist nur noch die Spalte Notiz (#32)

### Version 4.5.0

* Fehler am Wochenende wird die Mittagspause abgezogen behoben (#31)

### Version 4.4.0

* Fehler in der Berechnung der Statistiken behoben (#29)
* Einstellung um den Ort in den Zeiten auszublenden (#30)
* Feedback für Benutzer optimiert

### Version 4.3.0

* Optimierungen und Fehlerbehebungen

### Version 4.2.0

* Teilen von Zeiten möglich (#15)
* Optimierungen und Fehlerbehebungen 

### Version 4.1.0

* Erweiterung der Zeiten um einen Ort (#28)
* Optimierungen und Fehlerbehebungen

### Version 4.0.0

* Komplett neuer Unterbau (#24)
* Fehlerbehandlung bei zwei Ereignissen am gleichen Tag (#26)

### Version 3.6.0

* Einstellung um das Fenster beim Start automatisch zu öffnen (#22)
* Speichern der letzten Fensterposition und -größe (#16)
* Korrektur in den Statistiken - Spalte "Anteil gebucht" (#25)

### Version 3.5.0

* Gebucht Haken kann nun auch in der Zeiten Liste gesetzt werden (#23)
