# Time Tracker

Windows App um Dienstleistungsstunden bequem und schnell zu erfassen.

## Changelog

[Changelog](Changelog.md)

## Lizenz

[GNU GPLv3](Licence.md)