﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using time_tracker.Helper;

namespace time_tracker_test
{
    [TestClass]
    public class TimeCalcTest
    {
        [TestMethod]
        public void Test_getMonthStr()
        {
            string monthCorrect = TimeCalc.getMonthStr(1);
            Assert.AreEqual("Januar", monthCorrect);

            string monthFalse = TimeCalc.getMonthStr(-1);
            Assert.AreEqual("-1", monthFalse);
        }
    }
}
