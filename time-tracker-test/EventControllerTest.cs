﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using time_tracker.Controller;

namespace time_tracker_test
{
    [TestClass]
    public class EventControllerTest
    {
        [TestMethod]
        public void Test_getEvents()
        {
            EventController eventController = new EventController();
            var events = eventController.GetAll();
            Assert.IsTrue(events.Count > 0);
        }
    }
}
